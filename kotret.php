KONTRAK PEMBUATAN APLIKASI TRY OUT CPNS

1. PIHAK yang TERLIBAT
1.1 Pembuat : [Nama Pembuat]
Alamat: [Alamat Pembuat]
Email: [Alamat Email Pembuat]
Telepon: [Nomor Telepon Pembuat]

1.2 Klien: [Nama Klien]
Alamat: [Alamat Klien]
Email: [Alamat Email Klien]
Telepon: [Nomor Telepon Klien]

2. DESKRIPSI PROYEK
2.1 Pembuatan Aplikasi Web untuk Tryout CPNS
Proyek ini bertujuan untuk mengembangkan aplikasi web yang dapat digunakan untuk tryout Calon Pegawai Negeri Sipil (CPNS). Aplikasi ini akan menyediakan platform yang memungkinkan pengguna untuk melakukan simulasi ujian sesuai dengan format dan kurikulum yang berlaku untuk tes CPNS. Aplikasi ini dirancang untuk membantu para peserta tryout mempersiapkan diri dengan baik melalui latihan soal-soal yang mirip dengan tes CPNS sesungguhnya.

2.2 Spesifikasi Teknis
- Bahasa Pemrograman:
  - HTML, CSS, JavaScript untuk tampilan dan interaksi pengguna.
  - PHP sebagai bahasa pemrograman backend.
- Framework:
  - Laravel sebagai framework utama untuk pengembangan web.
- Database:
  - MySQL sebagai sistem manajemen basis data.
- Fitur Utama:
  - Autentikasi Pengguna:
    - Registrasi pengguna dengan email.
    - Proses pendaftaran melibatkan pembayaran sebagai salah satu langkah.
    - Pengguna diharuskan mengirimkan bukti pembayaran, dan admin akan memvalidasi pembayaran sebelum mengaktifkan akun.
  - Bank Soal:
    - Soal-soal diimpor melalui file Excel atau CSV.
    - Penambahan, pengeditan, dan penghapusan soal oleh administrator.
  - Simulasi Ujian:
    - Pengguna dapat memilih kategori dan jumlah soal untuk simulasi ujian.
    - Waktu terbatas dan sistem penilaian otomatis setelah pengguna menyelesaikan ujian.
  - Laporan Hasil Ujian:
    - Pengguna dapat melihat dan mengunduh laporan hasil ujian mereka.
    - Perbandingan hasil dengan peserta lainnya (opsional).
	- Rekap hasil ujian akan dikirim ke kontak orang tua melalui platform Whatsapp 
  - Interaksi Forum:
    - Forum diskusi untuk pengguna berbagi pengalaman dan kiat persiapan.
    - Pengguna dapat mengajukan pertanyaan kepada administrator.
  - Pengelolaan Akun Administrator:
    - Panel admin untuk manajemen pengguna, bank soal, dan hasil ujian.
    - Pemantauan aktivitas pengguna dan statistik aplikasi.

Aplikasi ini akan dikembangkan dengan fokus pada keamanan, responsivitas, dan kemudahan penggunaan. Fitur-fitur lainnya dapat ditambahkan sesuai dengan kebutuhan dan perkembangan proyek. Seluruh kode sumber dan dokumentasi teknis akan diserahkan kepada klien setelah penyelesaian proyek.

3. BATASAN WAKTU
3.1 Proyek ini akan diselesaikan dalam waktu 2 bulan sejak tanggal dimulainya proyek.

4. HONORARIUM DAN PEMBAYARAN
4.1 Klien setuju untuk membayar [Jumlah Honorarium] kepada Pembuat untuk penyelesaian proyek.

4.2 Pembayaran akan dilakukan dalam [Jumlah Pembayaran Pecahan] sebagai berikut:
   a. [Persentase] setelah penandatanganan kontrak (muka).
   b. [Persentase] setelah penyelesaian tahap tertentu:
      - Tahap ini mencakup [jelaskan tahap spesifik, misalnya, desain atau pengembangan inti].
   c. [Persentase] setelah penyelesaian proyek dan persetujuan dari Klien.
      - Pembayaran ini tergantung pada penerimaan akhir proyek oleh Klien.

5. TANGGUNG JAWAB PEMBUAT
5.1 Pembuat akan berusaha sebaik mungkin untuk menyelesaikan proyek sesuai dengan spesifikasi yang telah disepakati.
   5.1.1 Pembuat bertanggung jawab untuk mengikuti standar pengembangan yang baik dan memastikan bahwa hasil akhir memenuhi persyaratan fungsional dan desain yang telah ditentukan.
   5.1.2 Pembuat akan secara proaktif mengidentifikasi dan mengatasi hambatan atau kendala yang mungkin muncul selama pengembangan proyek.
   5.1.3 Jika terdapat perubahan yang diperlukan terhadap spesifikasi, Pembuat akan mengkomunikasikannya kepada Klien dan membahas opsi serta implikasi perubahan tersebut.

5.2 Pembuat akan memberikan pembaruan berkala kepada Klien mengenai perkembangan proyek.
   5.2.1 Pembuat akan menyediakan laporan kemajuan secara rutin, mingguan, yang mencakup status pengembangan, pencapaian, dan perencanaan ke depan.
   5.2.2 Pembuat akan dengan segera memberi tahu Klien jika terjadi keterlambatan atau perubahan signifikan dalam proyek, dan akan menyertakan solusi atau rencana pemulihan yang diusulkan.

6. TANGGUNG JAWAB KLIEN
6.1 Klien akan menyediakan semua informasi dan materi yang diperlukan untuk penyelesaian proyek.
   6.1.1 Klien harus menyediakan akses yang diperlukan ke sistem atau sumber daya yang relevan agar Pembuat dapat melaksanakan tugas dengan efisien.
   6.1.2 Klien akan memberikan data dan informasi yang diperlukan secara tepat waktu, termasuk tetapi tidak terbatas pada konten, gambar, dan spesifikasi tambahan.

6.2 Klien akan memberikan umpan balik yang konstruktif dan tepat waktu selama pengembangan proyek.
   6.2.1 Klien diharapkan memberikan tanggapan reguler terhadap deliverables yang disajikan oleh Pembuat.
   6.2.2 Klien akan memberikan umpan balik yang jelas dan konstruktif untuk memastikan bahwa proyek bergerak sesuai harapan.
   6.2.3 Jika Klien memerlukan perubahan atau memiliki kekhawatiran, Klien akan menyampaikannya dengan segera agar dapat diatasi dengan efektif.

7. PERUBAHAN LINGKUP
7.1 Perubahan pada lingkup proyek harus disetujui secara tertulis oleh kedua belah pihak.
   7.1.1 Sebelum melakukan perubahan lingkup, pihak yang mengusulkan perubahan harus menyediakan dokumentasi yang jelas dan rinci tentang perubahan yang diinginkan, termasuk dampaknya terhadap jadwal dan biaya.
   7.1.2 Setelah menerima proposal perubahan, kedua belah pihak akan membahas implikasi perubahan tersebut terhadap waktu penyelesaian, biaya, dan sumber daya lainnya.
   7.1.3 Perubahan lingkup hanya dapat dilaksanakan setelah kedua belah pihak mencapai kesepakatan tertulis mengenai perubahan tersebut.
   7.1.4 Jika perubahan lingkup berdampak pada biaya dan waktu, pembuat akan menyajikan perubahan rincian biaya dan jadwal yang diperbarui, dan klien akan menyetujuinya secara tertulis sebelum pelaksanaan perubahan.

8. PELANGGARAN KONTRAK
8.1 Jika salah satu pihak melanggar kontrak ini, pihak yang lain dapat menghentikan kontrak dan menuntut ganti rugi.
   8.1.1 Pelanggaran kontrak mencakup, tetapi tidak terbatas pada, kegagalan memenuhi tenggat waktu yang telah ditetapkan, tidak mematuhi spesifikasi proyek, atau pelanggaran ketentuan kontrak lainnya.
   8.1.2 Pihak yang merasa dirugikan oleh pelanggaran kontrak akan memberikan pemberitahuan tertulis kepada pihak pelanggar, memberikan waktu yang wajar untuk perbaikan, dan memberikan kesempatan untuk membahas atau menyelesaikan sengketa.
   8.1.3 Jika pelanggaran tersebut tidak dapat diperbaiki atau diselesaikan dengan baik dalam batas waktu yang telah ditetapkan, pihak yang merasa dirugikan berhak untuk menghentikan kontrak dan mengajukan tuntutan ganti rugi sesuai dengan kerugian yang diakibatkan.
   8.1.4 Pembubaran kontrak akan dilakukan setelah pemberitahuan tertulis dan pertimbangan yang wajar telah diberikan kepada pihak pelanggar.
   8.1.5 Seluruh ketentuan di atas tidak membatasi hak-hak atau remedies lain yang mungkin tersedia menurut hukum yang berlaku.

9. Penandatanganan
Dengan menandatangani kontrak ini, kedua belah pihak setuju untuk mematuhi semua ketentuan dan kondisi yang tercantum di dalamnya.

Pembuat (Pemberi Kerja): ________________________
Tanggal: ________________________

Klien: ________________________
Tanggal: ________________________

