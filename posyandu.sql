-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 05, 2023 at 06:45 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `posyandu`
--

-- --------------------------------------------------------

--
-- Table structure for table `childs`
--

CREATE TABLE `childs` (
  `id` int NOT NULL,
  `id_parent` int DEFAULT NULL,
  `Nama` varchar(255) NOT NULL,
  `code` varchar(20) NOT NULL,
  `Tanggal_lahir` date DEFAULT NULL,
  `Berat_badan_lahir` decimal(5,2) DEFAULT NULL,
  `Panjang_badan_lahir` decimal(5,2) DEFAULT NULL,
  `Jenis_kelamin` enum('P','L') DEFAULT NULL,
  `is_on` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `childs`
--

INSERT INTO `childs` (`id`, `id_parent`, `Nama`, `code`, `Tanggal_lahir`, `Berat_badan_lahir`, `Panjang_badan_lahir`, `Jenis_kelamin`, `is_on`) VALUES
(1, 7, 'Adam', 'h6tu7', '2020-10-06', '3.20', '50.50', 'L', 0),
(2, 2, 'Eva', '67887', '2020-08-20', '3.50', '51.00', 'P', 0),
(3, 2, 'David', '', '2019-11-05', '3.70', '48.20', 'L', 0),
(4, 3, 'Sophia', '', '2020-06-10', '3.40', '49.00', 'P', 1),
(5, 2, 'Ava', 'yt757t576', '2020-01-28', '3.60', '50.20', 'P', 0),
(6, 8, 'dika', '', '2023-10-30', '32.00', '32.00', 'P', 0),
(9, 2, 'Dave', '', '2023-10-16', '999.99', '43.00', 'P', 0),
(10, 9, 'hj', '', '0009-08-07', '50.00', '6.00', 'P', 0),
(11, 2, 'jlkl', '7576', '2023-11-22', '70.00', '60.00', 'P', 0);

-- --------------------------------------------------------

--
-- Table structure for table `height_nutrients`
--

CREATE TABLE `height_nutrients` (
  `id` int NOT NULL,
  `Usia` int DEFAULT NULL,
  `Tinggi_awal_laki` int DEFAULT NULL,
  `Tinggi_akhir_laki` int DEFAULT NULL,
  `Keterangan_Pendek_laki` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Keterangan_Normal_laki` varchar(255) DEFAULT NULL,
  `Keterangan_Tinggi_laki` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Tinggi_awal_perempuan` int DEFAULT NULL,
  `Tinggi_akhir_perempuan` int DEFAULT NULL,
  `Keterangan_Pendek_perempuan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Keterangan_Normal_perempuan` varchar(255) DEFAULT NULL,
  `Keterangan_Tinggi_perempuan` varchar(50) DEFAULT NULL,
  `Gizi_Pendek_laki` varchar(50) DEFAULT NULL,
  `Gizi_Normal_laki` varchar(50) DEFAULT NULL,
  `Gizi_Tinggi_laki` varchar(50) DEFAULT NULL,
  `Gizi_Pendek_perempuan` varchar(50) DEFAULT NULL,
  `Gizi_Normal_perempuan` varchar(50) DEFAULT NULL,
  `Gizi_Tinggi_perempuan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `height_nutrients`
--

INSERT INTO `height_nutrients` (`id`, `Usia`, `Tinggi_awal_laki`, `Tinggi_akhir_laki`, `Keterangan_Pendek_laki`, `Keterangan_Normal_laki`, `Keterangan_Tinggi_laki`, `Tinggi_awal_perempuan`, `Tinggi_akhir_perempuan`, `Keterangan_Pendek_perempuan`, `Keterangan_Normal_perempuan`, `Keterangan_Tinggi_perempuan`, `Gizi_Pendek_laki`, `Gizi_Normal_laki`, `Gizi_Tinggi_laki`, `Gizi_Pendek_perempuan`, `Gizi_Normal_perempuan`, `Gizi_Tinggi_perempuan`) VALUES
(1, 1, 70, 75, 'Kurang dari normal', 'Normal', 'Normal', 65, 70, 'Kurang dari normal', 'Normal', 'Normal', 'dsds', 'rerleklre', NULL, NULL, NULL, NULL),
(2, 2, 71, 76, 'Kurang dari normal', 'Normal', 'Normal', 66, 71, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 72, 77, 'Kurang dari normal', 'Normal', 'Normal', 67, 72, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 73, 150, 'Kurang dari normal', 'Normal', 'Normal', 68, 73, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 5, 74, 79, 'Kurang dari normal', 'Normal', 'Normal', 69, 74, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 6, 75, 80, 'Kurang dari normal', 'Normal', 'Normal', 70, 75, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 7, 76, 81, 'Kurang dari normal', 'Normal', 'Normal', 71, 76, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 8, 77, 82, 'Kurang dari normal', 'Normal', 'Normal', 72, 77, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 9, 78, 83, 'Kurang dari normal', 'Normal', 'Normal', 73, 78, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 10, 79, 84, 'Kurang dari normal', 'Normal', 'Normal', 74, 79, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 11, 80, 85, 'Kurang dari normal', 'Normal', 'Normal', 75, 80, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(12, 12, 81, 86, 'Kurang dari normal', 'Normal', 'Normal', 76, 81, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(13, 13, 82, 87, 'Kurang dari normal', 'Normal', 'Normal', 77, 82, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(14, 14, 83, 299, 'Kurang dari normal', 'Normal', 'Normal', 78, 83, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(15, 15, 84, 89, 'Kurang dari normal', 'Normal', 'Normal', 79, 84, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 16, 85, 90, 'Kurang dari normal', 'Normal', 'Normal', 80, 85, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 17, 86, 91, 'Kurang dari normal', 'Normal', 'Normal', 81, 86, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 18, 87, 92, 'Kurang dari normal', 'Normal', 'Normal', 82, 87, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 19, 88, 93, 'Kurang dari normal', 'Normal', 'Normal', 83, 88, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 20, 89, 94, 'Kurang dari normal', 'Normal', 'Normal', 84, 89, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 21, 90, 95, 'Kurang dari normal', 'Normal', 'Normal', 85, 90, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 22, 91, 96, 'Kurang dari normal', 'Normal', 'Normal', 86, 91, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 23, 92, 97, 'Kurang dari normal', 'Normal', 'Normal', 87, 92, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 24, 93, 98, 'Kurang dari normal', 'Normal', 'Normal', 88, 93, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 25, 94, 99, 'Kurang dari normal', 'Normal', 'Normal', 89, 94, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(26, 26, 95, 100, 'Kurang dari normal', 'Normal', 'Normal', 90, 95, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(27, 27, 96, 101, 'Kurang dari normal', 'Normal', 'Normal', 91, 96, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(28, 28, 97, 102, 'Kurang dari normal', 'Normal', 'Normal', 92, 97, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(29, 29, 98, 103, 'Kurang dari normal', 'Normal', 'Normal', 93, 98, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(30, 30, 99, 104, 'Kurang dari normal', 'Normal', 'Normal', 94, 99, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(31, 31, 110, 115, 'Kurang dari normal', 'Normal', 'Normal', 105, 110, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(32, 32, 111, 116, 'Kurang dari normal', 'Normal', 'Normal', 106, 111, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(33, 33, 112, 117, 'Kurang dari normal', 'Normal', 'Normal', 107, 112, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(34, 34, 113, 118, 'Kurang dari normal', 'Normal', 'Normal', 108, 113, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(35, 35, 114, 119, 'Kurang dari normal', 'Normal', 'Normal', 109, 114, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(36, 36, 115, 120, 'Kurang dari normal', 'Normal', 'Normal', 110, 115, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(37, 37, 116, 121, 'Kurang dari normal', 'Normal', 'Normal', 111, 116, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(38, 38, 117, 122, 'Kurang dari normal', 'Normal', 'Normal', 112, 117, 'Kurang dari normal', 'Normal', 'Normal', 'minum susu', 'minum susu', 'minum susu', 'minum susu', 'minum susu', 'minum susu'),
(39, 39, 118, 123, 'Kurang dari normal', 'Normal', 'Normal', 113, 118, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(40, 40, 119, 124, 'Kurang dari normal', 'Normal', 'Normal', 114, 119, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(41, 41, 120, 125, 'Kurang dari normal', 'Normal', 'Normal', 115, 120, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(42, 42, 121, 126, 'Kurang dari normal', 'Normal', 'Normal', 116, 121, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(43, 43, 122, 127, 'Kurang dari normal', 'Normal', 'Normal', 117, 122, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(44, 44, 123, 128, 'Kurang dari normal', 'Normal', 'Normal', 118, 123, 'Kurang dari normal', 'Normal', 'Normal', 'harus banyak minum susu', NULL, NULL, 'minum susu', NULL, NULL),
(45, 45, 124, 129, 'Kurang dari normal', 'Normal', 'Normal', 119, 124, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(46, 46, 125, 130, 'Kurang dari normal', 'Normal', 'Normal', 120, 125, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(47, 47, 126, 131, 'Kurang dari normal', 'Normal', 'Normal', 121, 126, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(48, 48, 127, 132, 'Kurang dari normal', 'Normal', 'Normal', 122, 127, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(49, 49, 128, 133, 'Kurang dari normal', 'Normal', 'Normal', 123, 128, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(50, 50, 129, 134, 'Kurang dari normal', 'Normal', 'Normal', 124, 129, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(51, 51, 130, 135, 'Kurang dari normal', 'Normal', 'Normal', 125, 130, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(52, 52, 131, 136, 'Kurang dari normal', 'Normal', 'Normal', 126, 131, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(53, 53, 132, 137, 'Kurang dari normal', 'Normal', 'Normal', 127, 132, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(54, 54, 133, 138, 'Kurang dari normal', 'Normal', 'Normal', 128, 133, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(55, 55, 134, 139, 'Kurang dari normal', 'Normal', 'Normal', 129, 134, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(56, 56, 135, 140, 'Kurang dari normal', 'Normal', 'Normal', 130, 135, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(57, 57, 136, 141, 'Kurang dari normal', 'Normal', 'Normal', 131, 136, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(58, 58, 137, 142, 'Kurang dari normal', 'Normal', 'Normal', 132, 137, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(59, 59, 138, 143, 'Kurang dari normal', 'Normal', 'Normal', 133, 138, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL),
(60, 60, 139, 144, 'Kurang dari normal', 'Normal', 'Normal', 134, 139, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

CREATE TABLE `histories` (
  `id` int NOT NULL,
  `id_child` int NOT NULL,
  `usia` int DEFAULT NULL,
  `panjang` decimal(5,2) DEFAULT NULL,
  `berat` decimal(5,2) DEFAULT NULL,
  `weight_result` enum('<','=','>') DEFAULT NULL,
  `height_result` enum('<','=','>') DEFAULT NULL,
  `ideal_result` enum('<','=','>') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `histories`
--

INSERT INTO `histories` (`id`, `id_child`, `usia`, `panjang`, `berat`, `weight_result`, `height_result`, `ideal_result`, `waktu`) VALUES
(1, 3, 47, '175.00', '80.00', '>', '<', '>', '2023-10-30 04:13:33'),
(2, 5, 45, '175.00', '40.00', '>', '<', '>', '2023-10-30 08:03:33'),
(3, 5, 60, '0.00', '0.00', '<', '<', '', '2023-11-09 08:07:22');

-- --------------------------------------------------------

--
-- Table structure for table `ideal_nutrients`
--

CREATE TABLE `ideal_nutrients` (
  `id` int NOT NULL,
  `Tinggi` int DEFAULT NULL,
  `Berat_awal_laki` int DEFAULT NULL,
  `Berat_akhir_laki` int DEFAULT NULL,
  `Keterangan_Kurus_laki` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Keterangan_Normal_laki` varchar(255) DEFAULT NULL,
  `Keterangan_Gendut_laki` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Berat_akhir_perempuan` int DEFAULT NULL,
  `Berat_awal_perempuan` int DEFAULT NULL,
  `Keterangan_Kurus_perempuan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Keterangan_Normal_perempuan` varchar(255) DEFAULT NULL,
  `Keterangan_Gendut_perempuan` varchar(50) DEFAULT NULL,
    `Gizi_Kurus_perempuan` varchar(50) DEFAULT NULL,
  `Gizi_Normal_perempuan` varchar(50) DEFAULT NULL,
  `Gizi_Gendut_perempuan` varchar(50) DEFAULT NULL,
    `Gizi_Kurus_laki` varchar(255) DEFAULT NULL,
  `Gizi_Normal_laki` varchar(255) DEFAULT NULL,
  `Gizi_Gendut_laki` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ideal_nutrients`
--

INSERT INTO `ideal_nutrients` (`id`, `Tinggi`, `Berat_awal_laki`, `Berat_akhir_laki`, `Keterangan_Kurus_laki`, `Keterangan_Normal_laki`, `Keterangan_Gendut_laki`, `Berat_akhir_perempuan`, `Berat_awal_perempuan`, `Keterangan_Kurus_perempuan`, `Keterangan_Normal_perempuan`, `Keterangan_Gendut_perempuan`) VALUES
(1, 1, 3, 6, 'Kurang dari normal', 'Normal', 'Normal', 6, 3, 'Kurang dari normal', 'Normal', 'Normal'),
(2, 6, 4, 7, 'Kurang dari normal', 'Normal', 'Normal', 7, 4, 'Kurang dari normal', 'Normal', 'Normal'),
(3, 11, 5, 8, 'Kurang dari normal', 'Normal', 'Normal', 8, 5, 'Kurang dari normal', 'Normal', 'Normal'),
(4, 16, 6, 9, 'Kurang dari normal', 'Normal', 'Normal', 9, 6, 'Kurang dari normal', 'Normal', 'Normal'),
(5, 5, 7, 10, 'Kurang dari normal', 'Normal', 'Normal', 10, 7, 'Kurang dari normal', 'Normal', 'Normal'),
(6, 6, 8, 11, 'Kurang dari normal', 'Normal', 'Normal', 11, 8, 'Kurang dari normal', 'Normal', 'Normal'),
(7, 7, 9, 12, 'Kurang dari normal', 'Normal', 'Normal', 12, 9, 'Kurang dari normal', 'Normal', 'Normal'),
(8, 8, 10, 13, 'Kurang dari normal', 'Normal', 'Normal', 13, 10, 'Kurang dari normal', 'Normal', 'Normal'),
(9, 9, 11, 14, 'Kurang dari normal', 'Normal', 'Normal', 14, 11, 'Kurang dari normal', 'Normal', 'Normal'),
(10, 10, 12, 15, 'Kurang dari normal', 'Normal', 'Normal', 15, 12, 'Kurang dari normal', 'Normal', 'Normal'),
(11, 11, 13, 16, 'Kurang dari normal', 'Normal', 'Normal', 16, 13, 'Kurang dari normal', 'Normal', 'Normal'),
(12, 12, 14, 17, 'Kurang dari normal', 'Normal', 'Normal', 17, 14, 'Kurang dari normal', 'Normal', 'Normal'),
(13, 13, 15, 18, 'Kurang dari normal', 'Normal', 'Normal', 18, 15, 'Kurang dari normal', 'Normal', 'Normal'),
(14, 14, 16, 19, 'Kurang dari normal', 'Normal', 'Normal', 19, 16, 'Kurang dari normal', 'Normal', 'Normal'),
(15, 15, 17, 20, 'Kurang dari normal', 'Normal', 'Normal', 20, 17, 'Kurang dari normal', 'Normal', 'Normal'),
(16, 16, 18, 21, 'Kurang dari normal', 'Normal', 'Normal', 21, 18, 'Kurang dari normal', 'Normal', 'Normal'),
(17, 17, 19, 22, 'Kurang dari normal', 'Normal', 'Normal', 22, 19, 'Kurang dari normal', 'Normal', 'Normal'),
(18, 18, 20, 23, 'Kurang dari normal', 'Normal', 'Normal', 23, 20, 'Kurang dari normal', 'Normal', 'Normal'),
(19, 19, 21, 24, 'Kurang dari normal', 'Normal', 'Normal', 24, 21, 'Kurang dari normal', 'Normal', 'Normal'),
(20, 20, 22, 25, 'Kurang dari normal', 'Normal', 'Normal', 25, 22, 'Kurang dari normal', 'Normal', 'Normal'),
(21, 21, 23, 26, 'Kurang dari normal', 'Normal', 'Normal', 26, 23, 'Kurang dari normal', 'Normal', 'Normal'),
(22, 22, 24, 27, 'Kurang dari normal', 'Normal', 'Normal', 27, 24, 'Kurang dari normal', 'Normal', 'Normal'),
(23, 23, 25, 28, 'Kurang dari normal', 'Normal', 'Normal', 28, 25, 'Kurang dari normal', 'Normal', 'Normal'),
(24, 24, 26, 29, 'Kurang dari normal', 'Normal', 'Normal', 29, 26, 'Kurang dari normal', 'Normal', 'Normal'),
(25, 25, 27, 30, 'Kurang dari normal', 'Normal', 'Normal', 30, 27, 'Kurang dari normal', 'Normal', 'Normal'),
(26, 26, 28, 31, 'Kurang dari normal', 'Normal', 'Normal', 31, 28, 'Kurang dari normal', 'Normal', 'Normal'),
(27, 27, 29, 30, 'Kurang dari normal', 'Normal', 'Normal', 33, 29, 'Kurang dari normal', 'Normal', 'Normal'),
(28, 28, 30, 34, 'Kurang dari normal', 'Normal', 'Normal', 34, 30, 'Kurang dari normal', 'Normal', 'Normal'),
(29, 29, 31, 35, 'Kurang dari normal', 'Normal', 'Normal', 35, 31, 'Kurang dari normal', 'Normal', 'Normal'),
(30, 30, 32, 36, 'Kurang dari normal', 'Normal', 'Normal', 36, 32, 'Kurang dari normal', 'Normal', 'Normal'),
(31, 31, 33, 36, 'Kurang dari normal', 'Normal', 'Normal', 36, 33, 'Kurang dari normal', 'Normal', 'Normal'),
(32, 32, 34, 37, 'Kurang dari normal', 'Normal', 'Normal', 37, 34, 'Kurang dari normal', 'Normal', 'Normal'),
(33, 33, 35, 38, 'Kurang dari normal', 'Normal', 'Normal', 38, 35, 'Kurang dari normal', 'Normal', 'Normal'),
(34, 34, 36, 39, 'Kurang dari normal', 'Normal', 'Normal', 39, 36, 'Kurang dari normal', 'Normal', 'Normal'),
(35, 35, 37, 40, 'Kurang dari normal', 'Normal', 'Normal', 40, 37, 'Kurang dari normal', 'Normal', 'Normal'),
(36, 36, 38, 41, 'Kurang dari normal', 'Normal', 'Normal', 41, 38, 'Kurang dari normal', 'Normal', 'Normal'),
(37, 37, 39, 42, 'Kurang dari normal', 'Normal', 'Normal', 42, 39, 'Kurang dari normal', 'Normal', 'Normal'),
(38, 38, 40, 43, 'Kurang dari normal', 'Normal', 'Normal', 43, 40, 'Kurang dari normal', 'Normal', 'Normal'),
(39, 39, 41, 44, 'Kurang dari normal', 'Normal', 'Normal', 44, 41, 'Kurang dari normal', 'Normal', 'Normal'),
(40, 40, 42, 45, 'Kurang dari normal', 'Normal', 'Normal', 45, 42, 'Kurang dari normal', 'Normal', 'Normal'),
(41, 41, 43, 46, 'Kurang dari normal', 'Normal', 'Normal', 46, 43, 'Kurang dari normal', 'Normal', 'Normal'),
(42, 42, 44, 47, 'Kurang dari normal', 'Normal', 'Normal', 47, 44, 'Kurang dari normal', 'Normal', 'Normal'),
(43, 43, 45, 48, 'Kurang dari normal', 'Normal', 'Normal', 48, 45, 'Kurang dari normal', 'Normal', 'Normal'),
(44, 44, 46, 49, 'Kurang dari normal', 'Normal', 'Normal', 49, 46, 'Kurang dari normal', 'Normal', 'Normal'),
(45, 45, 47, 50, 'Kurang dari normal', 'Normal', 'Normal', 50, 47, 'Kurang dari normal', 'Normal', 'Normal'),
(46, 46, 48, 51, 'Kurang dari normal', 'Normal', 'Normal', 51, 48, 'Kurang dari normal', 'Normal', 'Normal'),
(47, 47, 49, 52, 'Kurang dari normal', 'Normal', 'Normal', 52, 49, 'Kurang dari normal', 'Normal', 'Normal'),
(48, 48, 50, 53, 'Kurang dari normal', 'Normal', 'Normal', 53, 50, 'Kurang dari normal', 'Normal', 'Normal'),
(49, 49, 51, 54, 'Kurang dari normal', 'Normal', 'Normal', 54, 51, 'Kurang dari normal', 'Normal', 'Normal'),
(50, 50, 52, 55, 'Kurang dari normal', 'Normal', 'Normal', 55, 52, 'Kurang dari normal', 'Normal', 'Normal'),
(51, 51, 53, 56, 'Kurang dari normal', 'Normal', 'Normal', 56, 53, 'Kurang dari normal', 'Normal', 'Normal'),
(52, 52, 54, 57, 'Kurang dari normal', 'Normal', 'Normal', 57, 54, 'Kurang dari normal', 'Normal', 'Normal'),
(53, 53, 55, 58, 'Kurang dari normal', 'Normal', 'Normal', 58, 55, 'Kurang dari normal', 'Normal', 'Normal'),
(54, 54, 56, 59, 'Kurang dari normal', 'Normal', 'Normal', 59, 56, 'Kurang dari normal', 'Normal', 'Normal'),
(55, 55, 57, 60, 'Kurang dari normal', 'Normal', 'Normal', 60, 57, 'Kurang dari normal', 'Normal', 'Normal'),
(56, 56, 58, 61, 'Kurang dari normal', 'Normal', 'Normal', 61, 58, 'Kurang dari normal', 'Normal', 'Normal'),
(57, 57, 59, 62, 'Kurang dari normal', 'Normal', 'Normal', 62, 59, 'Kurang dari normal', 'Normal', 'Normal'),
(58, 58, 60, 63, 'Kurang dari normal', 'Normal', 'Normal', 63, 60, 'Kurang dari normal', 'Normal', 'Normal'),
(59, 59, 61, 64, 'Kurang dari normal', 'Normal', 'Normal', 64, 61, 'Kurang dari normal', 'Normal', 'Normal'),
(60, 60, 62, 65, 'Kurang dari normal', 'Normal', 'Normal', 65, 62, 'Kurang dari normal', 'Normal', 'Normal'),
(61, 543, 6646, 5465, '46455', '6464', '4664', 456, 56546, '64', '445', '46554'),
(63, 543, 6646, 5465, '46455', '6464', '4664', 456, 56546, '64', '445', '46554'),
(65, 543, 6646, 5465, '46455', '6464', '4664', 456, 56546, '64', '445', '46554'),
(67, 70, 645, 64, '645', '654', '54', 654, 654, '64', '654', '65'),
(68, 70, 645, 64, '645', '654', '54', 654, 654, '64', '654', '65');

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE `parents` (
  `id` int NOT NULL,
  `id_user` int DEFAULT NULL,
  `Nama_ayah` varchar(255) DEFAULT NULL,
  `Nama_ibu` varchar(255) DEFAULT NULL,
  `Alamat` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`id`, `id_user`, `Nama_ayah`, `Nama_ibu`, `Alamat`) VALUES
(1, 5, 'Yosir', 'Mia', '423jljkl'),
(2, 2, '89789798', '7987897', '8'),
(3, NULL, 'Hasyim', 'Yori', 'ok'),
(4, NULL, '3533', '3554', 'daa'),
(5, NULL, 'Yosir', 'Mia', '423'),
(6, NULL, 'terte', 'ert', 'retrt'),
(7, NULL, '78686', '868', '768686'),
(8, 7, 'fsfs', 'sdfsd', 'sf'),
(9, 9, 'Yuan', 'Yuani', '-');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'app_name', 'Posyandu Al-Hikmah'),
(2, 'address', 'Jl sum'),
(3, 'logo', 'icon_posyandu_(1).png'),
(4, 'hero_description', 'Selamat datang di Posyandu Al-Hikmah'),
(5, 'description', 'Posyandu Al-Hikmah adalah pusat kesehatan yang melibatkan partisipasi aktif dari masyarakat. Kami memberikan layanan kesehatan, pemantauan kesehatan, dan pendidikan gizi untuk meningkatkan kualitas hidup. Tujuan kami adalah mencapai kesejahteraan bersama.'),
(7, 'home_image', 'd');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) NOT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `role` enum('Parent','Admin') DEFAULT 'Parent'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `email`, `password`, `foto`, `role`) VALUES
(1, 'dads', 'admin@mail.com', '$2y$10$cWQ3m0nG1vSoaX5x9zBXyuGanV6eD6hxj4snENKPzgCUoMVphCvHy', 'R.jpg', 'Admin'),
(2, '677988', '86876868', '$2y$10$QoUJ7GfZVjPAz6FmU8Gz8OgJ.KG.Kz78/AgmmVBUI1gF22yhO6o2W', NULL, 'Parent'),
(3, 'yori', 'yori@mail.com', '$2y$10$4e/2BuZ2HYiWIWqHS39JQOOe2G54Cq12eKu71c.RX3Wu6s7Eqn24q', NULL, 'Parent'),
(4, 'TESTING ADMINNN', 'tesadmin@mail.com', '$2y$10$PwFfj6sgyhG/r1zi6IURkemJCszfDxn9c2pIMK7Df1dBANn4YwmIy', 'Screenshot_(3).png', NULL),
(5, 'Nimata', 'nimata@mail.com', '$2y$10$FnMD9lha0sgNjnNCRacG0ufjVwPwpvat3WVZMoc./GFekTD1n9rX.', 'Screenshot_(3)1.png', NULL),
(6, 'hlim', 'halim@mail.com', '$2y$10$rffFVJfGhR0JEomdpyi5F.FYHzcfAG7Xomq0Md5FcmH5IoCNmq7AC', 'Screenshot_(1).png', 'Admin'),
(7, 'halim@mail.com', 'halim@mail.com', '$2y$10$GA7qb5v3KuWWw7IVwi5I2eBovCCHZm8a55MZ.tUmtC6jk76S/JwzK', 'Screenshot_(19).png', 'Parent'),
(8, '54654', 'halim@mail.com546546', '$2y$10$0QbdHYXLDMCK7UXDWHf6/ef7t7uC9w5ZJYGwX3MOua.eZxIOMQZfa', 'Screenshot_(3)2.png', 'Parent'),
(9, 'yuan', 'yuan@mail.com', '$2y$10$jCNaQ19QIC4wrLS/k1hMt.kSlAGNy1LoFVGCoAYmzuABw3tZgkEou', 'icon_posyandu_(1).png', 'Parent');

-- --------------------------------------------------------

--
-- Table structure for table `weight_nutrients`
--

CREATE TABLE `weight_nutrients` (
  `id` int NOT NULL,
  `Usia` int DEFAULT NULL,
  `Berat_awal_laki` int DEFAULT NULL,
  `Berat_akhir_laki` int DEFAULT NULL,
  `Keterangan_Kurus_laki` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Gizi_Kurus_laki` varchar(255) DEFAULT NULL,
  `Gizi_Normal_laki` varchar(255) DEFAULT NULL,
  `Gizi_Gendut_laki` varchar(255) DEFAULT NULL,
  `Keterangan_Normal_laki` varchar(255) DEFAULT NULL,
  `Keterangan_Gendut_laki` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Berat_akhir_perempuan` int DEFAULT NULL,
  `Berat_awal_perempuan` int DEFAULT NULL,
  `Keterangan_Kurus_perempuan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Keterangan_Normal_perempuan` varchar(255) DEFAULT NULL,
  `Keterangan_Gendut_perempuan` varchar(50) DEFAULT NULL,
  `Gizi_Kurus_perempuan` varchar(50) DEFAULT NULL,
  `Gizi_Normal_perempuan` varchar(50) DEFAULT NULL,
  `Gizi_Gendut_perempuan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `weight_nutrients`
--

INSERT INTO `weight_nutrients` (`id`, `Usia`, `Berat_awal_laki`, `Berat_akhir_laki`, `Keterangan_Kurus_laki`, `Gizi_Kurus_laki`, `Gizi_Normal_laki`, `Gizi_Gendut_laki`, `Keterangan_Normal_laki`, `Keterangan_Gendut_laki`, `Berat_akhir_perempuan`, `Berat_awal_perempuan`, `Keterangan_Kurus_perempuan`, `Keterangan_Normal_perempuan`, `Keterangan_Gendut_perempuan`, `Gizi_Kurus_perempuan`, `Gizi_Normal_perempuan`, `Gizi_Gendut_perempuan`) VALUES
(1, 1, 1, 3, 'Kurang daritrtrnormal', 'makaknsanksna', NULL, NULL, 'Normal', 'Normal', 6, 3, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(2, 2, 4, 7, 'Kurang dari normal', NULL, NULL, 'minuman', 'Normal', 'Normal', 7, 4, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(3, 3, 5, 8, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 8, 5, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(4, 4, 6, 9, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 9, 6, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(5, 5, 7, 10, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 10, 7, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(6, 6, 8, 11, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 11, 8, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(7, 7, 9, 12, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 12, 9, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(8, 8, 10, 13, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 13, 10, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(9, 9, 11, 14, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 14, 11, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(10, 10, 12, 15, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 15, 12, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(11, 11, 13, 16, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 16, 13, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(12, 12, 14, 17, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 17, 14, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(13, 13, 15, 18, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 18, 15, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(14, 14, 16, 19, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 19, 16, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(15, 15, 17, 20, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 20, 17, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(16, 16, 18, 21, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 21, 18, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(17, 17, 19, 22, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 22, 19, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(18, 18, 20, 23, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 23, 20, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(19, 19, 21, 24, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 24, 21, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(20, 20, 22, 25, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 25, 22, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(21, 21, 23, 26, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 26, 23, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(22, 22, 24, 27, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 27, 24, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(23, 23, 25, 28, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 28, 25, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(24, 24, 26, 29, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 29, 26, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(25, 25, 27, 30, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 30, 27, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(26, 26, 28, 31, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 31, 28, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(27, 27, 29, 30, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 33, 29, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(28, 28, 30, 34, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 34, 30, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(29, 29, 31, 35, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 35, 31, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(30, 30, 32, 36, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 36, 32, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(31, 31, 33, 36, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 36, 33, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(32, 32, 34, 37, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 37, 34, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(33, 33, 35, 38, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 38, 35, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(34, 34, 36, 39, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 39, 36, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(35, 35, 37, 40, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 40, 37, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(36, 36, 38, 41, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 41, 38, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(37, 37, 39, 42, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 42, 39, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(38, 38, 40, 43, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 43, 40, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(39, 39, 41, 44, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 44, 41, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(40, 40, 42, 45, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 45, 42, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(41, 41, 43, 46, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 46, 43, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(42, 42, 44, 47, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 47, 44, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(43, 43, 45, 48, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 48, 45, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(44, 44, 46, 49, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 49, 46, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(45, 45, 47, 50, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 50, 47, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(46, 46, 48, 51, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 51, 48, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(47, 47, 49, 52, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'gemuk', 52, 49, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(48, 48, 50, 53, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 53, 50, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(49, 49, 51, 54, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 54, 51, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(50, 50, 52, 55, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 55, 52, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(51, 51, 53, 56, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 56, 53, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(52, 52, 54, 57, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 57, 54, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(53, 53, 55, 58, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 58, 55, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(54, 54, 56, 59, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 59, 56, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(55, 55, 57, 60, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 60, 57, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(56, 56, 58, 61, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 61, 58, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(57, 57, 59, 62, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 62, 59, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(58, 58, 60, 63, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 63, 60, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(59, 59, 61, 64, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 64, 61, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL),
(60, 60, 62, 65, 'Kurang dari normal', NULL, NULL, NULL, 'Normal', 'Normal', 65, 62, 'Kurang dari normal', 'Normal', 'Normal', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `childs`
--
ALTER TABLE `childs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_parent` (`id_parent`);

--
-- Indexes for table `height_nutrients`
--
ALTER TABLE `height_nutrients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_child` (`id_child`);

--
-- Indexes for table `ideal_nutrients`
--
ALTER TABLE `ideal_nutrients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `weight_nutrients`
--
ALTER TABLE `weight_nutrients`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `childs`
--
ALTER TABLE `childs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `height_nutrients`
--
ALTER TABLE `height_nutrients`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `histories`
--
ALTER TABLE `histories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ideal_nutrients`
--
ALTER TABLE `ideal_nutrients`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `parents`
--
ALTER TABLE `parents`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `weight_nutrients`
--
ALTER TABLE `weight_nutrients`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `childs`
--
ALTER TABLE `childs`
  ADD CONSTRAINT `childs_ibfk_1` FOREIGN KEY (`id_parent`) REFERENCES `parents` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `histories`
--
ALTER TABLE `histories`
  ADD CONSTRAINT `histories_ibfk_1` FOREIGN KEY (`id_child`) REFERENCES `childs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `parents`
--
ALTER TABLE `parents`
  ADD CONSTRAINT `parents_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
