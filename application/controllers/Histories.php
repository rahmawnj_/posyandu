<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Histories extends CI_Controller
{
	public function index()
	{
		$this->db->select('histories.id, histories.id_child, histories.usia, histories.panjang, waktu, histories.berat, histories.weight_result, histories.height_result,histories.ideal_result, childs.Nama as child_name,code,childs.Tanggal_lahir as tanggal_lahir, childs.Jenis_kelamin as gender');
		$this->db->from('histories');
		$this->db->join('childs', 'histories.id_child = childs.id');
		if ($this->session->userdata('role') == 'Parent') {
			$parent_id = $this->session->userdata('parent_id'); // Gantilah dengan ID orang tua yang ingin Anda tampilkan datanya
			$this->db->where('childs.id_parent', $parent_id); // Menambahkan kondisi WHERE berdasarkan id orang tua
		}
		$this->db->order_by('histories.waktu', 'DESC'); // Mengurutkan berdasarkan tanggal kunjungan descending

		$histories = $this->db->get()->result();

		$this->load->view('dashboard/histories/index', [
			'histories' => $histories
		]);
	}

	public function tambah_history_anak()
	{
		// Ambil berat badan dan panjang badan dari POST request
		$berat_badan = $this->input->post('berat');
		$panjang_badan = $this->input->post('panjang');
		$id_anak = $this->input->post('child_id');
		$usia = $this->input->post('usia');


		// Cari anak yang is_on = 1 (aktif)
		$this->db->select('*');
		$this->db->from('childs');
		$this->db->where('id', $id_anak);
		$child = $this->db->get()->row();


		// Hitung umur anak dalam bulan
		// 		$tanggal_lahir = new DateTime($child->Tanggal_lahir);
		// 		$sekarang = new DateTime('now');
		// 		$perbedaan = $sekarang->diff($tanggal_lahir);
		// 		$umur_bulan = $perbedaan->y * 12 + $perbedaan->m;

		$umur_bulan = $usia;

		// Tentukan jenis kelamin anak
		$jenis_kelamin = $child->Jenis_kelamin;

		// Cari data gizi dari tabel weight_nutrients
		$this->db->select('*');
		$this->db->from('weight_nutrients');
		$this->db->where('Usia', $umur_bulan);
		$gizi_weight = $this->db->get()->row();

		// Cari data gizi dari tabel height_nutrients
		$this->db->select('*');
		$this->db->from('height_nutrients');
		$this->db->where('Usia', $umur_bulan);
		$gizi_height = $this->db->get()->row();

		// Cari data gizi dari tabel ideal_nutrients berdasarkan tinggi
		$this->db->select('*');
		$this->db->from('ideal_nutrients');
		$this->db->where('Tinggi <=', $panjang_badan); // Mencari tinggi yang lebih besar atau sama dengan panjang_badan
		$ideal_nutrient = $this->db->get()->row();

		// Tentukan status gizi berdasarkan berat badan dan tinggi badan
		$status_gizi_weight = '';
		$status_gizi_height = '';
		$status_gizi_ideal = '';

		$result_weight = '';
		$result_height = '';
		$result_ideal = '';
		if ($jenis_kelamin == 'L') {
			if ($berat_badan < $gizi_weight->Berat_awal_laki) {
				$status_gizi_weight = '<';
				$result_weight = 'Kurus';
			} elseif ($berat_badan >= $gizi_weight->Berat_awal_laki && $berat_badan <= $gizi_weight->Berat_akhir_laki) {
				$result_weight = 'Normal';
				$status_gizi_weight = '=';
			} else {
				$result_weight = 'Gendut';
				$status_gizi_weight = '>';
			}

			if ($panjang_badan < $gizi_height->Tinggi_awal_laki) {
				$status_gizi_height = '<';
				$result_height = 'Pendek';
			} elseif ($panjang_badan >= $gizi_height->Tinggi_awal_laki && $panjang_badan <= $gizi_height->Tinggi_akhir_laki) {
				$status_gizi_height = '=';
				$result_height = 'Normal';
			} else {
				$status_gizi_height = '>';
				$result_height = 'Tinggi';
			}
		} elseif ($jenis_kelamin == 'P') {
			if ($berat_badan < $gizi_weight->Berat_awal_perempuan) {
				$status_gizi_weight = '<';
				$result_weight = 'Kurus';
			} elseif ($berat_badan >= $gizi_weight->Berat_awal_perempuan && $berat_badan <= $gizi_weight->Berat_akhir_perempuan) {
				$status_gizi_weight = '=';
				$result_weight = 'Normal';
			} else {
				$result_weight = 'Gendut';

				$status_gizi_weight = '>';
			}

			if ($panjang_badan < $gizi_height->Tinggi_awal_perempuan) {
				$status_gizi_height = '<';
				$result_height = 'Pendek';
			} elseif ($panjang_badan >= $gizi_height->Tinggi_awal_perempuan && $panjang_badan <= $gizi_height->Tinggi_akhir_perempuan) {
				$status_gizi_height = '=';
				$result_height = 'Normal';
			} else {
				$status_gizi_height = '>';
				$result_height = 'Tinggi';
			}
		}

		// Jika data ideal nutrient ditemukan, tentukan status gizi ideal
		if ($ideal_nutrient) {
			if ($berat_badan < $ideal_nutrient->Berat_awal_laki) {
				$status_gizi_ideal = '<';
				$result_ideal = 'kurus';
			} elseif ($berat_badan > $ideal_nutrient->Berat_akhir_laki) {
				$status_gizi_ideal = '>';
				$result_ideal = 'gendut';
			} else {
				$status_gizi_ideal = '=';
				$result_ideal = 'normal';
			}
		}

		// Menggabungkan status gizi berdasarkan berat dan tinggi

		// Menyimpan status gizi dan keterangan ke dalam tabel histories
		$data_history = [
			'id_child' => $child->id,
			'usia' => $umur_bulan,
			'panjang' => $panjang_badan,
			'berat' => $berat_badan,
			'weight_result' => $status_gizi_weight,
			'height_result' => $status_gizi_height,
			'ideal_result' => $status_gizi_ideal,
			'waktu' => date('Y-m-d H:i:s'), // Menambahkan tanggal dan waktu saat ini
		];

		// Pengecekan jika sudah ada catatan pada usia yang sama
		$this->db->select('*');
		$this->db->from('histories');
		$this->db->where('id_child', $child->id);
		$this->db->where('usia', $umur_bulan);
		$existing_history = $this->db->get()->row();

		if ($existing_history) {
			// Jika catatan sudah ada, lakukan pembaruan
			$this->db->where('id', $existing_history->id);
			$this->db->update('histories', $data_history);
		} else {
			// Jika catatan belum ada, buat catatan baru
			$this->db->insert('histories', $data_history);
		}

		// Ubah status is_on anak menjadi 0
		$this->db->where('id', $child->id);
		$this->db->update('childs', ['is_on' => 0]);

		return redirect('/histories');
	}
}
