<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public function index2()
	{
		
		// Mengambil data riwayat kesehatan anak-anak
		// $this->db->select('ideal_result');
		$this->db->select('*');
		$this->db->join('childs', 'childs.id = histories.id_child');
		$this->db->where('histories.waktu >=', date('Y-m-d', strtotime('-1 month')));
		$histories = $this->db->get('histories')->result();


		// Inisialisasi variabel untuk menghitung persentase
		$totalAnak = count($histories);
		$kurus = $normal = $gendut = 0;

		foreach ($histories as $history) {
			$idealResult = $history->ideal_result;

			// Periksa apakah hasil sesuai dengan ideal nutrisi
			if ($idealResult === '<') {
				$kurus++;
			} elseif ($idealResult === '=') {
				$normal++;
			} elseif ($idealResult === '>') {
				$gendut++;
			}
		}

		$jumlahHistoryPerempuan = 0;
		$jumlahHistoryLakiLaki = 0;

		foreach ($histories as $history) {
			$gender = $history->Jenis_kelamin;
			if ($gender === 'L') {
				$jumlahHistoryLakiLaki++;
			} elseif ($gender === 'P') {
				$jumlahHistoryPerempuan++;
			}
		}


		$persentaseKurus = ($kurus / $totalAnak) * 100;
		$persentaseNormal = ($normal / $totalAnak) * 100;
		$persentaseGendut = ($gendut / $totalAnak) * 100;

		$anak = $this->db
			->select('Jenis_kelamin')
			->where("DATEDIFF(NOW(), Tanggal_lahir) <= (60 * 30)")
			->get('childs')
			->result();

		$jumlah_laki_laki = 0;
		$jumlah_perempuan = 0;

		foreach ($anak as $anak) {
			if ($anak->Jenis_kelamin === 'L') {
				$jumlah_laki_laki++;
			} elseif ($anak->Jenis_kelamin === 'P') {
				$jumlah_perempuan++;
			}
		}

		$data = [
			'persentaseKurus' => $persentaseKurus,
			'persentaseNormal' => $persentaseNormal,
			'persentaseGendut' => $persentaseGendut,
			'jumlahHistoryPerempuan' => $jumlahHistoryPerempuan,
			'jumlahHistoryLakiLaki' => $jumlahHistoryLakiLaki,
			'jumlah_perempuan' => $jumlah_perempuan,
			'jumlah_laki_laki' => $jumlah_laki_laki,
			'histories' => $histories
		];

		$this->load->view('dashboard/index', $data);
	}

	public function index()
	{
		$this->load->library('auth');
		$this->auth->checkLogin();

		$this->db->select('*');
		$this->db->join('childs', 'childs.id = histories.id_child');
		$histories = $this->db->get('histories')->result();
		$totalWeight = 0;
		$totalHeight = 0;
		$totalBMI = 0;
		$totalChildren = count($histories);

		foreach ($histories as $history) {
			$totalWeight += $history->berat;
			$totalHeight += $history->panjang;
			// Hitung BMI sesuai dengan rumus yang sesuai
			$totalBMI += ($history->berat / (($history->panjang / 100) * ($history->panjang / 100)));
		}

if ($totalChildren > 0) {
    $averageWeight = $totalWeight / $totalChildren;
    $averageHeight = $totalHeight / $totalChildren;
    $averageBMI = $totalBMI / $totalChildren;
} else {
    // Handle the case where there are no records in $histories (totalChildren is zero)
    // You can set these averages to a default value or handle it as needed.
    $averageWeight = 0;
    $averageHeight = 0;
    $averageBMI = 0;
}

	$data = array(
    'averageWeight' => (int)$averageWeight,
    'averageHeight' => (int)$averageHeight,
    'averageBMI' => (int)$averageBMI,
    'histories' => $histories
);

		$this->load->view('dashboard/index', $data);
	}
}
