<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{
	public function __construct()
	{
		parent::__construct();
	}
	public function check_anak_get()
	{
		// Cari anak yang is_on = 1 (aktif)
		$this->db->from('childs');
		$this->db->where('is_on', 1);
		$child = $this->db->get()->row();

		if (!$child) {
			$this->response([
				'status' => 'Tidak ada anak',
			], 200); // Not Found
		} else {
			$tanggal_lahir = new DateTime($child->Tanggal_lahir);
			$sekarang = new DateTime('now');
			$perbedaan = $sekarang->diff($tanggal_lahir);
			$umur_bulan = $perbedaan->y * 12 + $perbedaan->m;
			// Mengembalikan ID anak yang aktif
			$this->response([
				'status' => 'success',
				'id_anak' => $child->id,
				'nama' => strtolower($child->Nama),
				'usia' => $umur_bulan
			], 200);
		}
	}



	public function gizi_post()
	{
		// Ambil berat badan dan panjang badan dari POST request
		$berat_badan = $this->post('berat');
		$panjang_badan = $this->post('panjang');
		$id_anak = $this->post('id_anak');

		// Cari data gizi dari tabel ideal_nutrients berdasarkan tinggi
		if (!$berat_badan || !$panjang_badan) {
			$this->response([
				'status' => 'Berat badan dan panjang kosong',
			], 200); // Bad Request
		}

		// Cari anak yang is_on = 1 (aktif)
		$this->db->select('*');
		$this->db->from('childs');
		$this->db->where('id', $id_anak);
		$child = $this->db->get()->row();

		if ($child->is_on != 1) {
			$this->response([
				'status' => 'id_anak ' . $id_anak . ' tidak aktif.',
			], 200); // Not Found
		}

		// Hitung umur anak dalam bulan
		$tanggal_lahir = new DateTime($child->Tanggal_lahir);
		$sekarang = new DateTime('now');
		$perbedaan = $sekarang->diff($tanggal_lahir);
		$umur_bulan = $perbedaan->y * 12 + $perbedaan->m;

		// Tentukan jenis kelamin anak
		$jenis_kelamin = $child->Jenis_kelamin;

		// Cari data gizi dari tabel weight_nutrients
		$this->db->select('*');
		$this->db->from('weight_nutrients');
		$this->db->where('Usia', $umur_bulan);
		$gizi_weight = $this->db->get()->row();

		// Cari data gizi dari tabel height_nutrients
		$this->db->select('*');
		$this->db->from('height_nutrients');
		$this->db->where('Usia', $umur_bulan);
		$gizi_height = $this->db->get()->row();

		// Cari data gizi dari tabel ideal_nutrients berdasarkan tinggi
		$this->db->select('*');
		$this->db->from('ideal_nutrients');
		$this->db->where('Tinggi <=', $panjang_badan); // Mencari tinggi yang lebih besar atau sama dengan panjang_badan
		$ideal_nutrient = $this->db->get()->row();

		// Tentukan status gizi berdasarkan berat badan dan tinggi badan
		$status_gizi_weight = '';
		$status_gizi_height = '';
		$status_gizi_ideal = '';

		$result_weight = '';
		$result_height = '';
		$result_ideal = '';
		if ($jenis_kelamin == 'L') {
			if ($berat_badan < $gizi_weight->Berat_awal_laki) {
				$status_gizi_weight = '<';
				$result_weight = 'Kurus';
			} elseif ($berat_badan >= $gizi_weight->Berat_awal_laki && $berat_badan <= $gizi_weight->Berat_akhir_laki) {
				$result_weight = 'Normal';
				$status_gizi_weight = '=';
			} else {
				$result_weight = 'Gendut';
				$status_gizi_weight = '>';
			}

			if ($panjang_badan < $gizi_height->Tinggi_awal_laki) {
				$status_gizi_height = '<';
				$result_height = 'Pendek';
			} elseif ($panjang_badan >= $gizi_height->Tinggi_awal_laki && $panjang_badan <= $gizi_height->Tinggi_akhir_laki) {
				$status_gizi_height = '=';
				$result_height = 'Normal';
			} else {
				$status_gizi_height = '>';
				$result_height = 'Tinggi';
			}
		} elseif ($jenis_kelamin == 'P') {
			if ($berat_badan < $gizi_weight->Berat_awal_perempuan) {
				$status_gizi_weight = '<';
				$result_weight = 'Kurus';
			} elseif ($berat_badan >= $gizi_weight->Berat_awal_perempuan && $berat_badan <= $gizi_weight->Berat_akhir_perempuan) {
				$status_gizi_weight = '=';
				$result_weight = 'Normal';
			} else {
				$result_weight = 'Gendut';

				$status_gizi_weight = '>';
			}

			if ($panjang_badan < $gizi_height->Tinggi_awal_perempuan) {
				$status_gizi_height = '<';
				$result_height = 'Pendek';
			} elseif ($panjang_badan >= $gizi_height->Tinggi_awal_perempuan && $panjang_badan <= $gizi_height->Tinggi_akhir_perempuan) {
				$status_gizi_height = '=';
				$result_height = 'Normal';
			} else {
				$status_gizi_height = '>';
				$result_height = 'Tinggi';
			}
		}

		// Jika data ideal nutrient ditemukan, tentukan status gizi ideal
		if ($ideal_nutrient) {
			if ($berat_badan < $ideal_nutrient->Berat_awal_laki) {
				$status_gizi_ideal = '<';
				$result_ideal = 'kurus';
			} elseif ($berat_badan > $ideal_nutrient->Berat_akhir_laki) {
				$status_gizi_ideal = '>';
				$result_ideal = 'gendut';
			} else {
				$status_gizi_ideal = '=';
				$result_ideal = 'normal';
			}
		}

		// Menggabungkan status gizi berdasarkan berat dan tinggi

		// Menyimpan status gizi dan keterangan ke dalam tabel histories
		$data_history = [
			'id_child' => $child->id,
			'usia' => $umur_bulan,
			'panjang' => $panjang_badan,
			'berat' => $berat_badan,
			'weight_result' => $status_gizi_weight,
			'height_result' => $status_gizi_height,
			'ideal_result' => $status_gizi_ideal,
			'waktu' => date('Y-m-d H:i:s'), // Menambahkan tanggal dan waktu saat ini
		];

		// Pengecekan jika sudah ada catatan pada usia yang sama
		$this->db->select('*');
		$this->db->from('histories');
		$this->db->where('id_child', $child->id);
		$this->db->where('usia', $umur_bulan);
		$existing_history = $this->db->get()->row();

		if ($existing_history) {
			// Jika catatan sudah ada, lakukan pembaruan
			$this->db->where('id', $existing_history->id);
			$this->db->update('histories', $data_history);
		} else {
			// Jika catatan belum ada, buat catatan baru
			$this->db->insert('histories', $data_history);
		}
		
		// Ubah status is_on anak menjadi 0
		$this->db->where('id', $child->id);
		$this->db->update('childs', ['is_on' => 0]);

		$jenis_kelamin = ($jenis_kelamin == 'P') ? 'Perempuan' : 'Laki';
		// Buat respons API
		$response_data = [
			'status' => 'Data disimpan.',
			'umur_bulan' => $umur_bulan,
			'jenis_kelamin' => $jenis_kelamin,
			'status_berat' => $result_weight,
			'status_panjang' => $result_height,
			'status_berat_panjang' => $result_ideal,
			'nama' => $child->Nama,
			'usia' => $umur_bulan
		];

		$this->response($response_data, 200);
	}
}
