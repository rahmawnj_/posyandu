<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library("form_validation");
		$this->load->model(['Users_model', 'Parents_model']);
	}

	public function signin()
	{
		$this->load->view('auth/signin');
	}
	public function signup()
	{
		$this->load->view('auth/signup');
	}

	public function signin_store()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		// Lakukan validasi, seperti memeriksa apakah email dan password sesuai dengan database
		// Untuk keperluan contoh, kita akan melakukan validasi sederhana
		$user = $this->db->get_where('users', ['email' => $email])->row();

		if (!$user) {
			// Jika email tidak ditemukan dalam database
			$this->session->set_flashdata('signin_validation', 'Email tidak ditemukan');

			redirect('auth/signin');
		} elseif (!password_verify($password, $user->password)) {
			// Jika password tidak cocok
			$this->session->set_flashdata('signin_validation', 'Password salah');
			redirect('auth/signin');
		} else {
			// Autentikasi berhasil
			// Disimpan ke dalam sesi atau dibuatkan token sesuai kebutuhan
			$user_data = [
				'id' => $user->id, // ID user yang berhasil login
				'nama' => $user->nama,
				'email' => $user->email,
				'foto' => $user->foto,
				'role' => $user->role
			];

			// Jika peran adalah "parent," temukan ID orang tua yang sesuai dengan user ID
			if ($user->role === 'Parent') {
				$parent = $this->db->get_where('parents', ['id_user' => $user->id])->row();
				if ($parent) {
					$user_data['parent_id'] = $parent->id;
				}
			}


			$this->session->set_userdata($user_data);

			if ($user->role === 'Parent') {
				redirect('childs'); // Redirect to the 'childs' page
			}
			redirect('dashboard'); // Redirect to the admin dashboard

		}
	}

	public function signup_store()
	{
		// Tangkap data yang dikirimkan melalui formulir
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password_confirmation', 'Konfirmasi Password', 'required|matches[password]');
		$this->form_validation->set_rules('nama_ayah', 'Nama Ayah', 'required');
		$this->form_validation->set_rules('nama_ibu', 'Nama Ibu', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

		if ($this->form_validation->run() === false) {
			// Jika validasi gagal, tampilkan kembali halaman pendaftaran dengan pesan kesalahan
			$this->signup();
		} else {
			// Jika validasi berhasil, lanjutkan proses pendaftaran seperti sebelumnya
			$email = $this->input->post('email');
			$nama = $this->input->post('nama');
			$password = $this->input->post('password');
			$nama_ayah = $this->input->post('nama_ayah');
			$nama_ibu = $this->input->post('nama_ibu');
			$alamat = $this->input->post('alamat');

			// Simpan data ke dalam tabel 'users'
			$user_data = [
				'nama' => $nama,
				'email' => $email,
				'password' => password_hash($password, PASSWORD_DEFAULT),
				'role' => 'Parent' // Sesuaikan dengan peran yang sesuai
			];
			$this->db->insert('users', $user_data);
			$user_id = $this->db->insert_id();

			// Simpan data ke dalam tabel 'parents'
			$parent_data = [
				'id_user' => $user_id,
				'Nama_ayah' => $nama_ayah,
				'Nama_ibu' => $nama_ibu,
				'Alamat' => $alamat
			];
			$this->db->insert('parents', $parent_data);

			// Setelah pendaftaran berhasil, Anda dapat mengarahkan pengguna ke halaman login atau halaman lain yang sesuai.
			redirect('auth/signin');
		}
	}


	public function logout()
	{
		// Hapus semua data sesi
		$this->session->sess_destroy();

		// Redirect ke halaman login
		redirect('/');
	}

	// controllers/Auth.php

	public function profile()
	{
		$user_id = $this->session->userdata('id');

		// Mengambil data orang tua berdasarkan id_user
		$this->db->where('id_user', $user_id);
		$query = $this->db->get('parents');

		if ($query->num_rows() > 0) {
			$parent = $query->row();
		} else {
			$parent = array(); // Atau sesuaikan dengan cara Anda menangani ketika data orang tua tidak ada.
		}

		// Mengambil data pengguna (user)
		$user = $this->Users_model->get_user($user_id);

		$data = ['user' => $user, 'parent' => $parent];
		$this->load->view('auth/profile', $data);
	}


	public function submit_profile()
	{
		// Form validation rules
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');

		if ($this->form_validation->run() == FALSE) {
			// Form validation failed, reload the edit form with errors
			// $user = $this->Users_model->get_user($this->session->userdata('id'));
			// $this->load->view('auth/edit_profile', ['user' => $user]);

			$this->profile();
		} else {
			// Handle file upload
			$config['upload_path'] = './assets/upload/image/users/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size'] = 2048; // 2MB max file size

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				// Delete previous image file if exists
				$user = $this->Users_model->get_user($this->session->userdata('id'));
				if ($user->foto && file_exists('./assets/upload/image/users/' . $user->foto)) {
					unlink('./assets/upload/image/users/' . $user->foto);
				}

				$upload_data = $this->upload->data();
				$foto = $upload_data['file_name'];
			} else {
				// Keep the existing image if file upload fails
				$user = $this->Users_model->get_user($this->session->userdata('id'));
				$foto = $user->foto;
			}

			// Form validation passed, update data in the database
			$data = array(
				'nama' => $this->input->post('nama'),
				'email' => $this->input->post('email'),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'foto' => $foto
			);

			$user_data = [
				'nama' => $this->input->post('nama'),
				'email' => $this->input->post('email'),
				'foto' => $foto
			];
			$this->session->set_userdata($user_data);

			$this->Users_model->update_user($this->session->userdata('id'), $data);

			$data = array(
				'Nama_ayah' => $this->input->post('nama_ayah'),
				'Nama_ibu' => $this->input->post('nama_ibu'),
				'Alamat' => $this->input->post('alamat')
			);


			$this->Parents_model->update_parent($this->input->post('parent_id'), $data);

			// Redirect to the edit profile page after successful update
			redirect('auth/profile');
		}
	}
}
