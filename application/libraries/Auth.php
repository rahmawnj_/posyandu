<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth
{
	protected $CI;

	public function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->library('session');
	}

	public function checkLogin()
	{
		if (!$this->CI->session->userdata('id')) {
			// Pengguna tidak terautentikasi, arahkan ke halaman login
			redirect('auth/signin');
		}
	}

	public function isAdmin()
	{
		$role = $this->CI->session->userdata('role');
		if ($role != 'Admin') {
			redirect('errors/forbidden'); // Mengarahkan ke halaman 403
		}
	}
}
