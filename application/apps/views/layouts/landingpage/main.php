<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title><?= $this->db->get_where('settings', ['name' => 'app_name'])->row()->value ?><?= isset($title) ? ' | ' . $title : '' ?></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />

	<!-- ================== BEGIN core-css ================== -->
	<link href="<?= base_url('assets/lp/css/corporate/vendor.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/lp/css/corporate/app.min.css') ?>" rel="stylesheet" />
	<!-- ================== END core-css ================== -->
    <link rel="icon" type="image/png" href="<?= base_url('assets/upload/image/settings/' . $this->db->get_where('settings', ['name' => 'logo'])->row()->value ?? ''); ?>" />


	<?php foreach ($css_urls as $css_url) : ?>
		<link href="<?= $css_url ?>" rel="stylesheet" />
	<?php endforeach; ?>
</head>

<body>

	<div id="page-container">
		<?php $this->load->view('layouts/landingpage/header') ?>
		<?= $content ?>
		<?php $this->load->view('layouts/landingpage/theme-panel') ?>
		<?php $this->load->view('layouts/landingpage/footer') ?>
	</div>


	<script src="<?= base_url('assets/lp/js/corporate/vendor.min.js') ?>"></script>
	<script src="<?= base_url('assets/lp/js/corporate/app.min.js') ?>"></script>
	<?php foreach ($js_urls as $js_url) : ?>
		<script src="<?= $js_url ?>"></script>
	<?php endforeach; ?>
</body>

</html>
