<?php
$title = 'Master Anak';
$breadcrumb = ['Master Anak', 'Tambah Anak'];

$css_urls = [];

$js_urls = [base_url('assets/d/plugins/apexcharts/dist/apexcharts.min.js')];

ob_start();

?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
		<div id="chart"></div>
	</div>


</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>




<script>
	document.addEventListener("DOMContentLoaded", function() {
		var options = {
			chart: {
				height: 350,
				type: "line",
				stacked: false,
			},
			dataLabels: {
				enabled: false,
			},
			stroke: {
				width: [2, 2, 0],
			},
			series: [{
					name: "Normal Awal",
					type: "line",
					data: <?= json_encode($normal_awal) ?>,
				},
				{
					name: "Normal Akhir",
					type: "line",
					data: <?= json_encode($normal_akhir) ?>,
				},
				{
					name: "PB Anak",
					type: "bar",
					data: <?= json_encode($panjang_data) ?>,
				},
			],

			xaxis: {
				categories: <?= json_encode($categories) ?>,
				title: {
					text: "Usia (bulan)" // Ganti teks dengan judul yang Anda inginkan
				},
			},
			yaxis: {
				title: {
					text: "Tinggi (cm)" // Ganti teks dengan judul yang Anda inginkan
				},
			},
			legend: {
				position: "top",
			},
		};

		var chart = new ApexCharts(document.querySelector("#chart"), options);

		chart.render();
	});
</script>