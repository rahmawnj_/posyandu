<?php
$title = 'Pengaturan';
$breadcrumb = ['Pengaturan'];

$css_urls = [];
$js_urls = [];

ob_start();
?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
		<?php echo validation_errors(); ?>

		<?php echo form_open_multipart('settings/submit'); ?>
		<fieldset>
			<legend class="mb-3">Pengaturan</legend>
			<?php foreach ($settings as $setting) : ?>
				<div class="form-group">
					<!-- <label for="<?= $setting->name ?>" class="form-label"><?= ucfirst($setting->name) ?>:</label> -->
					<label for="<?= $setting->name ?>" class="form-label"><?= ucfirst(strtr($setting->name, '_', ' ')) ?>:</label>

					<?php if ($setting->name == 'logo') : ?>
						<input type="file" name="logo" class="form-control" id="<?= $setting->name ?>" accept="image/*" />
						<?php if ($setting->value) : ?>
							<img src="<?= base_url('assets/upload/image/settings/' . $setting->value); ?>" height="50" alt="Current Logo" />
						<?php endif; ?>
					<?php else : ?>
						<input type="text" name="<?= $setting->name ?>" value="<?php echo set_value($setting->name, $setting->value); ?>" class="form-control" id="<?= $setting->name ?>" placeholder="Enter <?= ucfirst($setting->name) ?>" />
					<?php endif; ?>
				</div>
			<?php endforeach; ?>

			<button type="submit" class="btn btn-primary me-2">Submit</button>
			<button type="button" class="btn btn-secondary" onclick="window.location.reload();">Cancel</button>
		</fieldset>
		<?php echo form_close(); ?>
	</div>
</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>
