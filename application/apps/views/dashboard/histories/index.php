<?php
$title = 'Riwayat';
$breadcrumb = ['Riwayat'];

$css_urls = [
	base_url('assets/d/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css'),
	base_url('assets/d/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css'),
	base_url('assets/d/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css'),
];

$js_urls = [
	base_url('assets/d/plugins/datatables.net/js/jquery.dataTables.min.js'),
	base_url('assets/d/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js'),
	base_url('assets/d/plugins/datatables.net-responsive/js/dataTables.responsive.min.js'),
	base_url('assets/d/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/dataTables.buttons.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.colVis.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.flash.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.html5.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.print.min.js'),
	base_url('assets/d/plugins/pdfmake/build/pdfmake.min.js'),
	base_url('assets/d/plugins/pdfmake/build/vfs_fonts.js'),
	base_url('assets/d/plugins/jszip/dist/jszip.min.js'),
	base_url('assets/d/js/demo/table-manage-buttons.demo.js'),
	base_url('assets/d/plugins/@highlightjs/cdn-assets/highlight.min.js'),
	base_url('assets/d/js/demo/render.highlight.js'),
];


ob_start();

?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
			<?php if ($this->session->userdata('role') == 'Admin') : ?>
				<button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal">
					Timbang Manual
				</button>
			<?php endif ?>



		</div>
	</div>
	<div class="panel-body">
		<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Input Timbang Manual</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">
						<form action="<?= base_url('histories/tambah_history_anak') ?>" method="post">
							<div class="form-group">
								<?php

								// $this->db->select('childs.*, parents.Nama_ibu as Nama_ibu');
								$this->db->from('childs');
								// $this->db->join('parents', 'childs.id_parent = parents.id', 'left');
								$childs = $this->db->get()->result();


								?>
								<label for="child_id">Pilih Anak:</label>
								<select name="child_id" id="child_id" class="form-control">
									<option value="">Pilih Anak</option>
									<?php foreach ($childs as $child) : ?>
										<option value="<?php echo $child->id; ?>"><?php echo $child->Nama; ?> - <?= $child->code ?></option>
									<?php endforeach; ?>
								</select>

							</div>
							<div class="form-group">
								<label for="usia">Usia:</label>
								<input type="number" name="usia" id="usia" class="form-control" placeholder="Masukkan Usia" min="0" max="60" />
							</div>
							<div class="form-group">
								<label for="panjang">Panjang & Berat:</label>
								<div class="input-group mb-3">
									<input type="text" name="panjang" id="panjang" class="form-control" placeholder="Panjang (cm)" aria-label="Panjang (cm)" aria-describedby="basic-addon1">
									<span class="input-group-text" id="basic-addon1">cm</span>
								</div>
								<div class="input-group mb-3">
									<input type="text" name="berat" id="berat" class="form-control" placeholder="Berat (kg)" aria-label="Berat (kg)" aria-describedby="basic-addon2">
									<span class="input-group-text" id="basic-addon2">kg</span>
								</div>
							</div>


							<br>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</form>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<table id="data-table" class="table table-striped table-bordered align-middle">
			<thead>
				<tr>
					<th width="1%"></th>
					<th class="text-nowrap">Nama</th>
					<th class="text-nowrap">Kode</th>
					<th class="text-nowrap">Gender</th>
					<th class="text-nowrap">Usia</th>
					<th class="text-nowrap">Panjang</th>
					<th class="text-nowrap">Berat</th>
					<th class="text-nowrap">Umur x Panjang</th>
					<th class="text-nowrap">Umur x Berat</th>
					<th class="text-nowrap">Panjang x Berat</th>
					<th class="text-nowrap"></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($histories as $key => $history) : ?>
					<?php
					$this->db->select('*');
					$this->db->from('weight_nutrients');
					$this->db->where('Usia', $history->usia);
					$gizi_weight = $this->db->get()->row();

					$this->db->select('*');
					$this->db->from('height_nutrients');
					$this->db->where('Usia', $history->usia);
					$gizi_height = $this->db->get()->row();

					// var_dump($gizi_height);
					// die;

					$this->db->select('*');
					$this->db->from('ideal_nutrients');
					$this->db->where('Tinggi <=', $history->panjang); // Mencari tinggi yang lebih besar atau sama dengan panjang_badan
					$ideal_nutrient = $this->db->get()->row();

					?>

					<tr>
						<td class="fw-bold text-inverse"><?= $key + 1 ?></td>
						<td><?= $history->child_name ?></td>
						<td><?= $history->code ?></td>
						<td><?= $history->gender ?></td>
						<td><?= $history->usia ?></td>
						<td><?= $history->panjang ?> cm</td>
						<td><?= $history->berat ?> kg</td>
						<td>
							<?php if ($history->weight_result == '<') : ?>
								<span class="badge bg-danger">
									<?= ($history->gender == 'P') ? $gizi_weight->Keterangan_Kurus_perempuan : $gizi_weight->Keterangan_Kurus_laki ?>
								</span>
							<?php elseif ($history->weight_result == '=') : ?>
								<span class="badge bg-success">
									<?= ($history->gender == 'P') ? $gizi_weight->Keterangan_Normal_perempuan : $gizi_weight->Keterangan_Normal_laki ?>
								</span>
							<?php elseif ($history->weight_result == '>') : ?>
								<span class="badge bg-warning">
									<?= ($history->gender == 'P') ? $gizi_weight->Keterangan_Gendut_perempuan : $gizi_weight->Keterangan_Gendut_laki ?>
								</span>
							<?php endif; ?>
						</td>
						<td>
							<?php if ($history->height_result == '<') : ?>
								<span class="badge bg-danger">
									<?= ($history->gender == 'P') ? $gizi_height->Keterangan_Pendek_perempuan : $gizi_height->Keterangan_Pendek_laki ?>
								</span>
							<?php elseif ($history->height_result == '=') : ?>
								<span class="badge bg-success">
									<?= ($history->gender == 'P') ? $gizi_height->Keterangan_Normal_perempuan : $gizi_height->Keterangan_Normal_laki ?>
								</span>
							<?php elseif ($history->height_result == '>') : ?>
								<span class="badge bg-warning">
									<?= ($history->gender == 'P') ? $gizi_height->Keterangan_Tinggi_perempuan : $gizi_height->Keterangan_Tinggi_laki ?>
								</span>
							<?php endif; ?>
						</td>

						<td>
							<?php if ($history->ideal_result == '<') : ?>
								<span class="badge bg-danger">
									<?= ($history->gender == 'P') ? $ideal_nutrient->Keterangan_Kurus_perempuan : $ideal_nutrient->Keterangan_Kurus_laki ?>
								</span>
							<?php elseif ($history->ideal_result == '=') : ?>
								<span class="badge bg-success">
									<?= ($history->gender == 'P') ? $ideal_nutrient->Keterangan_Normal_perempuan : $ideal_nutrient->Keterangan_Normal_laki ?>
								</span>
							<?php elseif ($history->ideal_result == '>') : ?>
								<span class="badge bg-warning">
									<?= ($history->gender == 'P') ? $ideal_nutrient->Keterangan_Gendut_perempuan : $ideal_nutrient->Keterangan_Gendut_laki ?>
								</span>
							<?php endif; ?>
						</td>

						<td>
							<!-- <a href="<?= site_url('histories/delete/' . $history->id); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this history?')"><i class="fas fa-trash-alt"></i></a> -->
							<!-- <a href="<?= site_url('histories/edit/' . $history->id); ?>" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a> -->
							<a href="#modal-dialog-<?= $history->id ?>" data-bs-toggle="modal" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
						</td>
					</tr>

					<div class="modal fade" id="modal-dialog-<?= $history->id ?>">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Riwayat Penimbangan</h4>
									<button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
								</div>
								<div class="modal-body">
									Waktu: <?= date('d-m-Y H:i:s', strtotime($history->waktu)) ?> <br>
									Tanggal Lahir: <?= date('d-m-Y', strtotime($history->tanggal_lahir)) ?> <br>

									Note Berat Badan : <?php if ($history->height_result == '<') : ?>
										<?= ($history->gender == 'P') ? $gizi_height->Gizi_Pendek_perempuan : $gizi_height->Gizi_Pendek_laki ?>
									<?php elseif ($history->height_result == '=') : ?>
										<?= ($history->gender == 'P') ? $gizi_height->Gizi_Normal_perempuan : $gizi_height->Gizi_Normal_laki ?>
									<?php elseif ($history->height_result == '>') : ?>
										<?= ($history->gender == 'P') ? $gizi_height->Gizi_Tinggi_perempuan : $gizi_height->Gizi_Tinggi_laki ?>
									<?php endif; ?> <br>

									Note Panjang Badan : <?php if ($history->weight_result == '<') : ?>
										<?= ($history->gender == 'P') ? $gizi_weight->Gizi_Kurus_perempuan : $gizi_weight->Gizi_Kurus_laki ?>
									<?php elseif ($history->weight_result == '=') : ?>
										<?= ($history->gender == 'P') ? $gizi_weight->Gizi_Normal_perempuan : $gizi_weight->Gizi_Normal_laki ?>
									<?php elseif ($history->weight_result == '>') : ?>
										<?= ($history->gender == 'P') ? $gizi_weight->Gizi_Gendut_perempuan : $gizi_weight->Gizi_Gendut_laki ?>
									<?php endif; ?> <br>
								</div>
								<div class="modal-footer">
									<a href="javascript:;" class="btn btn-white" data-bs-dismiss="modal">Close</a>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</tbody>
		</table>

	</div>
</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>

<script>
	$(document).ready(function() {
		// DataTable initialization
		var dataTable = $('#data-table').DataTable({
			// Add any DataTable configuration options you need
			dom: 'Bfrtip', // Add DataTables Buttons to the DOM
			buttons: [
				'copy', 'print' // Specify the buttons you want
			]
		});

	});
</script>