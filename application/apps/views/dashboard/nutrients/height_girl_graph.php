<?php
$title = 'Grafik Gizi';
$breadcrumb = ['Grafik Gizi', 'Tinggi', 'Perempuan'];
$css_urls = [];

$js_urls = [
	base_url('/assets/d/plugins/chart.js/dist/chart.min.js'),
];


ob_start();

?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div>
		<canvas style="background-color: pink;" id="line-chart"></canvas>
	</div>
</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls, 'breadcrumb' => $breadcrumb]);
?>

<script>
	Chart.defaults.font.family = 'Arial, sans-serif';
	Chart.defaults.backgroundColor = '#00ffbf';
	Chart.defaults.borderColor = 'black';
	Chart.defaults.color = '#000';

	var ctx = document.getElementById('line-chart').getContext('2d');
	var lineChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: [<?php
						$labels = [];
						foreach ($nutrients as $nutrient) {
							$labels[] = $nutrient['Usia'];
						}
						echo implode(',', $labels);
						?>],
			datasets: [{
				label: 'Pendek',
				borderColor: 'red',
				pointBackgroundColor: 'red',
				pointRadius: 2,
				borderWidth: 2,
				data: [<?php
						$data = [];
						foreach ($nutrients as $nutrient) {
							$tinggiAwalPerempuan = $nutrient['Tinggi_awal_perempuan'];
							$pendekPerempuan = $tinggiAwalPerempuan - 1;
							$data[] = $pendekPerempuan;
						}
						echo implode(',', $data);
						?>],
			}, {
				label: 'Normal',
				borderColor: 'green',
				pointBackgroundColor: 'green',
				pointRadius: 2,
				borderWidth: 2,
				data: [<?php
						$data = [];
						foreach ($nutrients as $nutrient) {
							$tinggiAwalPerempuan = $nutrient['Tinggi_awal_perempuan'];
							$tinggiAkhirPerempuan = $nutrient['Tinggi_akhir_perempuan'];
							$normalPerempuan = ($tinggiAwalPerempuan + $tinggiAkhirPerempuan) / 2;
							$data[] = $normalPerempuan;
						}
						echo implode(',', $data);
						?>]
			}, {
				label: 'Tinggi',
				borderColor: 'blue',
				pointBackgroundColor: 'blue',
				pointRadius: 2,
				borderWidth: 2,
				data: [<?php
						$data = [];
						foreach ($nutrients as $nutrient) {
							$tinggiAkhirPerempuan = $nutrient['Tinggi_akhir_perempuan'];
							$gendutPerempuan = $tinggiAkhirPerempuan + 1;
							$data[] = $gendutPerempuan;
						}
						echo implode(',', $data);
						?>]
			}]
		},
		options: {
			scales: {
				x: {
					title: {
						display: true,
						text: 'Usia (bulan)'
					}
				},
				y: {
					title: {
						display: true,
						text: 'Tinggi Badan'
					}
				}
			}
		}
	});
</script>
