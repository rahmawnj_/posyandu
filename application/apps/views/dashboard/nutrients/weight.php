<?php
$title = 'Gizi';
$breadcrumb = ['Master Gizi', 'Berat Badan'];

$css_urls = [
	base_url('assets/d/plugins/x-editable-bs4/dist/bootstrap4-editable/css/bootstrap-editable.css'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/address/address.css'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css'),
	base_url('assets/d/plugins/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css'),
	base_url('assets/d/plugins/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css'),
	base_url('assets/d/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'),
	base_url('assets/d/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css'),
	base_url('assets/d/plugins/select2/dist/css/select2.min.css'),
];

$js_urls = [
	base_url('https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js'),
	base_url('assets/d/plugins/jquery-migrate/dist/jquery-migrate.min.js'),
	base_url('assets/d/plugins/x-editable-bs4/dist/bootstrap4-editable/js/bootstrap-editable.min.js'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/address/address.js'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/typeaheadjs/lib/typeahead.js'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/typeaheadjs/typeaheadjs.js'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/wysihtml5/wysihtml5.js'),
	base_url('assets/d/plugins/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js'),
	base_url('assets/d/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'),
	base_url('assets/d/plugins/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js'),
	base_url('assets/d/plugins/select2/dist/js/select2.full.min.js'),
	base_url('assets/d/plugins/jquery-mockjax/dist/jquery.mockjax.min.js'),
	base_url('assets/d/plugins/moment/min/moment.min.js'),
];

ob_start();
?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="table-responsive">
		<table id="user" class="table table-bordered table-panel mb-0">
			<thead class="bg-purple text-white">
				<tr>
					<th width="2%">Umur (Bulan)</th>
					<th colspan="3" class="text-center">Laki-Laki</th>
					<th colspan="3" class="text-center">Perempuan</th>
				</tr>
				<tr class="text-center">
					<th></th>
					<th>Kurus (kg) &le;</th>
					<th>Normal (kg) </th>
					<th>&ge; Gendut (kg) </th>
					<th>Kurus (kg)  &le;</th>
					<th>Normal (kg) </th>
					<th>&ge; Gendut (kg) </th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($nutrients as $key => $nutrion) : ?>
					<tr>
						<td rowspan="3" class="bg-light"><?= $nutrion->Usia ?></td>
						<td>
							<span id="KurusLaki<?= $nutrion->id ?>"></span> <!-- Menampilkan nilai "Kurus" Laki-Laki -->
						</td>
						<td>
							<a href="#" class="editable" data-type="number" data-pk="<?= $nutrion->id ?>" data-name="Berat_awal_laki" data-title="Normal (Berat Awal)"><?= $nutrion->Berat_awal_laki ?></a> -
							<a href="#" class="editable" data-type="number" data-pk="<?= $nutrion->id ?>" data-name="Berat_akhir_laki" data-title="Normal (Berat Akhir)"><?= $nutrion->Berat_akhir_laki ?></a>
						</td>
						<td>
							<span id="GendutLaki<?= $nutrion->id ?>"></span> <!-- Menampilkan hasil perhitungan "Gendut" Laki-Laki -->
						</td>
						<td>
							<span id="KurusPerempuan<?= $nutrion->id ?>"></span> <!-- Menampilkan nilai "Kurus" Perempuan -->
						</td>
						<td>
							<a href="#" class="editable" data-type="number" data-pk="<?= $nutrion->id ?>" data-name="Berat_awal_perempuan" data-title="Normal (Berat Awal)"><?= $nutrion->Berat_awal_perempuan ?></a> -
							<a href="#" class="editable" data-type="number" data-pk="<?= $nutrion->id ?>" data-name="Berat_akhir_perempuan" data-title="Normal (Berat Akhir)"><?= $nutrion->Berat_akhir_perempuan ?></a>
						</td>
						<td>
							<span id="GendutPerempuan<?= $nutrion->id ?>"></span> <!-- Menampilkan hasil perhitungan "Gendut" Perempuan -->
						</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Kurus_laki" data-title="Keterangan"><?= $nutrion->Keterangan_Kurus_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Normal_laki" data-title="Keterangan"><?= $nutrion->Keterangan_Normal_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Gendut_laki" data-title="Keterangan"><?= $nutrion->Keterangan_Gendut_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Kurus_perempuan" data-title="Keterangan"><?= $nutrion->Keterangan_Kurus_perempuan ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Normal_perempuan" data-title="Keterangan"><?= $nutrion->Keterangan_Normal_perempuan ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Gendut_perempuan" data-title="Keterangan"><?= $nutrion->Keterangan_Gendut_perempuan ?></a>
						</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Kurus_laki" data-title="Gizi"><?= $nutrion->Gizi_Kurus_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Normal_laki" data-title="Gizi"><?= $nutrion->Gizi_Normal_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Gendut_laki" data-title="Gizi"><?= $nutrion->Gizi_Gendut_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Kurus_perempuan" data-title="Gizi"><?= $nutrion->Gizi_Kurus_perempuan ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Normal_perempuan" data-title="Gizi"><?= $nutrion->Gizi_Normal_perempuan ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Gendut_perempuan" data-title="Gizi"><?= $nutrion->Gizi_Gendut_perempuan ?></a>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>

<!-- JavaScript -->
<script>
	$(document).ready(function() {
		// Fungsi untuk menghitung nilai "Kurus" dan "Gendut" dan mengisi selnya
		function calculateAndFillValues() {
			<?php foreach ($nutrients as $key => $nutrion) : ?>
				// Ambil nilai "Normal (Berat Awal)" dan "Normal (Berat Akhir)" dari elemen saat halaman dimuat
				var beratAwalLaki<?= $nutrion->id ?> = <?= $nutrion->Berat_awal_laki ?>;
				var beratAkhirLaki<?= $nutrion->id ?> = <?= $nutrion->Berat_akhir_laki ?>;
				var beratAwalPerempuan<?= $nutrion->id ?> = <?= $nutrion->Berat_awal_perempuan ?>;
				var beratAkhirPerempuan<?= $nutrion->id ?> = <?= $nutrion->Berat_akhir_perempuan ?>;

				// Hitung "Kurus" berdasarkan nilai "Normal (Berat Awal)"
				var kurusLaki<?= $nutrion->id ?> = beratAwalLaki<?= $nutrion->id ?> - 1;
				var kurusPerempuan<?= $nutrion->id ?> = beratAwalPerempuan<?= $nutrion->id ?> - 1;

				// Hitung "Gendut" berdasarkan nilai "Normal (Berat Akhir)"
				var gendutLaki<?= $nutrion->id ?> = beratAkhirLaki<?= $nutrion->id ?> + 1;
				var gendutPerempuan<?= $nutrion->id ?> = beratAkhirPerempuan<?= $nutrion->id ?> + 1;

				// Isi sel "Kurus" dengan nilai yang dihitung
				document.getElementById("KurusLaki<?= $nutrion->id ?>").textContent = kurusLaki<?= $nutrion->id ?>;
				document.getElementById("KurusPerempuan<?= $nutrion->id ?>").textContent = kurusPerempuan<?= $nutrion->id ?>;

				// Isi sel "Gendut" dengan hasil perhitungan
				document.getElementById("GendutLaki<?= $nutrion->id ?>").textContent = gendutLaki<?= $nutrion->id ?>;
				document.getElementById("GendutPerempuan<?= $nutrion->id ?>").textContent = gendutPerempuan<?= $nutrion->id ?>;
			<?php endforeach ?>
		}

		// Panggil fungsi saat halaman dimuat
		calculateAndFillValues();

		// Aktifkan Editable Bootstrap
		$('.editable').editable({
			mode: 'inline',
			inputclass: 'form-control input-sm',
			url: '<?= base_url('nutrients/weight_store') ?>' // Ganti dengan URL yang sesuai untuk menyimpan data ke server
		});


	});
</script>
