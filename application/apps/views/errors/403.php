<?php
$title = 'Dashboard';
$breadcrumb = ['Dashboard', 'Anak'];
$css_urls = [];
$js_urls = [];

ob_start();
?>


<!-- BEGIN section -->
<div class="section">
	<div id="app" class="app">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<img height="300" src="https://thumbs.dreamstime.com/b/i-sorry-message-white-background-sad-boy-holding-poster-word-sorry-conceptual-handwritten-message-boy-word-sorry-114449870.jpg" alt="Access Denied" class="img-responsive">
				</div>
				<!-- Tulisan di kanan -->
				<div class="col-md-6">
					<h1>403 - Akses Ditolak</h1>
					<p>Maaf, Anda tidak memiliki izin untuk mengakses halaman ini.</p>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
$content = ob_get_clean();
$this->load->view('layouts/landingpage/main', ['content' => $content, 'title' => $title, 'js_urls' => $js_urls, 'css_urls' => $css_urls]);
?>
