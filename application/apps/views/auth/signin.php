<?php
$title = 'Login';
$css_urls = [];
$js_urls = [];

ob_start();
?>

<div class="section bg-purple">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-4">
				<div class="card">
					<div class="card-header"><?= $title ?? '' ?></div>
					<div class="card-body">

						<?php if ($this->session->flashdata('signin_validation')) : ?>
							<div class="alert alert-danger">
								<?= $this->session->flashdata('signin_validation') ?>
							</div>
						<?php endif; ?>

						<form method="post" action="<?= base_url('auth/signin_store') ?>">
							<?php if (!empty(validation_errors())) : ?>
								<div class="alert alert-danger"><?= validation_errors(); ?></div>
							<?php endif; ?>

							<div class="form-group my-4">
								<label for="email">Email</label>
								<input type="text" class="form-control" id="email" name="email" value="<?= set_value('email'); ?>" required>
							</div>

							<div class="form-group my-4">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="password" name="password" required>
							</div>

							<button type="submit" class="btn btn-primary btn-block">Login</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$content = ob_get_clean();
$this->load->view('layouts/landingpage/main', ['content' => $content, 'title' => $title, 'js_urls' => $js_urls, 'css_urls' => $css_urls]);
?>