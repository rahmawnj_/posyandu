<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->database(); // Load the database library
	}

	public function index()
	{
		// Retrieve all settings from the database
		$data['settings'] = $this->db->get('settings')->result();

		// Load the view to display all settings
		$this->load->view('dashboard/settings', $data);
	}

	// public function submit()
	// {
	// 	// Retrieve existing logo filename
	// 	$existing_logo = $this->db->where('name', 'logo')->get('settings')->row()->value;

	// 	// Handle file upload
	// 	$config['upload_path'] = './assets/upload/image/users/';
	// 	$config['allowed_types'] = 'gif|jpg|jpeg|png';
	// 	$config['max_size'] = 2048; // 2MB max file size

	// 	$this->load->library('upload', $config);

	// 	// Loop through all settings and update them in the database
	// 	foreach ($this->input->post() as $name => $value) {

	// 		if ($name === 'logo') {
	// 			$config['upload_path'] = './assets/upload/image/settings/';
	// 			$config['allowed_types'] = 'gif|jpg|jpeg|png';
	// 			$config['max_size'] = 2048; // 2MB max file size
	// 			$this->load->library('upload', $config);

	// 			if ($this->upload->do_upload('logo')) {
	// 				$upload_data = $this->upload->data();
	// 				$value = $upload_data['file_name'];
	// 				// if ($existing_logo) {
	// 				// 	unlink('./assets/upload/image/settings/' . $existing_logo);
	// 				// }

	// 				// var_dump($value);
	// 				// die;
	// 			}
	// 		}

	// 		$data = [
	// 			'value' => $value
	// 		];

	// 		$this->db->where('name', $name);
	// 		$this->db->update('settings', $data);
	// 	}

	// 	// Redirect back to the settings list or any other appropriate page
	// 	redirect('settings');
	// }

	public function submit()
	{
		// Validasi form
		$this->load->library('form_validation');
		$settings = $this->input->post();
		$logo = $this->db->get_where('settings', ['name' => 'logo'])->row()->value;

		// Jika validasi sukses, proses upload file logo
		if (!empty($_FILES['logo']['name'])) {
			$config['upload_path'] = './assets/upload/image/settings/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('logo')) {
				$error = $this->upload->display_errors();
				// Tampilkan pesan error jika gagal mengunggah file
				$this->load->view('dashboard/settings', compact('error'));
			} else {
				$upload_data = $this->upload->data();
				$logo_filename = $upload_data['file_name'];

				if (file_exists('./assets/upload/image/settings/' . $logo)) {
					unlink('./assets/upload/image/settings/' . $logo);
				}

			}
		} else {
			// Jika tidak ada file yang diunggah, gunakan logo yang sudah ada
			$logo_filename = $logo;
		}

		// Simpan data ke database
		foreach ($settings as $key => $value) {
			// Update data sesuai dengan kolom 'name' di tabel
			$data = array('value' => $value);
			$this->db->where('name', $key);
			$this->db->update('settings', $data);
		}

		// Update data untuk kolom 'logo' secara khusus
		$data = array('value' => $logo_filename);
		$this->db->where('name', 'logo');
		$this->db->update('settings', $data);

		// Redirect ke halaman pengaturan dengan pesan sukses
		redirect('settings');
	}
}
