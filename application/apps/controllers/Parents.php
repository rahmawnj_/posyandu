<?php

class Parents extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("form_validation");
		$this->load->model('Parents_model');
		
		$this->load->library('auth');
		$this->auth->checkLogin();
		$this->auth->isAdmin();
	}

	public function index()
	{
		$data['parents'] = $this->Parents_model->get_parents();
		$this->load->view('dashboard/parents/index', $data);
	}

	public function create()
	{
		$this->load->view('dashboard/parents/create');
	}

	public function store()
	{
		// Form validation rules
		$this->form_validation->set_rules('Nama_ayah', 'Nama Ayah', 'required');
		$this->form_validation->set_rules('Nama_ibu', 'Nama Ibu', 'required');
		$this->form_validation->set_rules('Alamat', 'Alamat', 'required');

		if ($this->form_validation->run() == FALSE) {
			// Form validation failed, reload the create form with errors
			$this->load->view('dashboard/parents/create');
		} else {
			// Form validation passed, insert data into the database
			$data = array(
				'id_user' => $this->input->post('id_user'), // Sesuaikan dengan id user yang sesuai
				'Nama_ayah' => $this->input->post('Nama_ayah'),
				'Nama_ibu' => $this->input->post('Nama_ibu'),
				'Alamat' => $this->input->post('Alamat'),
			);

			try {
				$this->Parents_model->create_parent($data);
				$this->session->set_flashdata('success', 'Orang Tua Berhasil ditambah');
			} catch (\Throwable $e) {
				$this->session->set_flashdata('error', 'An error occurred: ' . $e->getMessage());
				redirect('parents/create'); // Redirect back to the create form
			}

			// Redirect to the index page after successful insertion
			redirect('parents');
		}
	}

	public function edit($id)
	{
		$data['parent'] = $this->Parents_model->get_parent($id);
		$this->load->view('dashboard/parents/edit', $data);
	}

	public function update($id)
	{
		// Form validation rules
		$this->form_validation->set_rules('Nama_ayah', 'Nama Ayah', 'required');
		$this->form_validation->set_rules('Nama_ibu', 'Nama Ibu', 'required');
		$this->form_validation->set_rules('Alamat', 'Alamat', 'required');

		if ($this->form_validation->run() == FALSE) {
			// Form validation failed, reload the edit form with errors
			$data['parent'] = $this->Parents_model->get_parent($id);
			$this->load->view('dashboard/parents/edit', $data);
		} else {
			// Form validation passed, update data in the database
			$data = array(
				'id_user' => $this->input->post('id_user'), // Sesuaikan dengan id user yang sesuai
				'Nama_ayah' => $this->input->post('Nama_ayah'),
				'Nama_ibu' => $this->input->post('Nama_ibu'),
				'Alamat' => $this->input->post('Alamat'),
			);

			try {
				$this->Parents_model->update_parent($id, $data);
				$this->session->set_flashdata('success', 'Orang Tua Berhasil diubah');
			} catch (\Throwable $e) {
				$this->session->set_flashdata('error', 'An error occurred: ' . $e->getMessage());
				redirect('parents/edit'); // Redirect back to the create form
			}

			// Redirect to the index page after successful update
			redirect('parents');
		}
	}

	public function delete($id)
	{
		try {
			$this->Parents_model->delete_parent($id);
			$this->session->set_flashdata('success', 'Orang Tua Berhasil dihapus');
		} catch (\Throwable $e) {
			$this->session->set_flashdata('error', 'An error occurred: ' . $e->getMessage());
		}

		redirect('parents');
	}
}
