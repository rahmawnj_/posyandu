<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Errors extends CI_Controller
{
	public function forbidden()
	{
		$this->output->set_status_header(403); // Set HTTP response code ke 403
		$this->load->view('errors/403'); // Tampilkan halaman 403 kustom
	}
	public function notfound()
	{
		$this->output->set_status_header(404); // Set HTTP response code ke 404
		$this->load->view('errors/404'); // Tampilkan halaman 403 kustom
	}
}
