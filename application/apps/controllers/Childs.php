<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Childs extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('auth');

		$this->load->library("form_validation");
		$this->load->model('Childs_model');

		$this->auth->checkLogin();
	}

	public function index()
	{

		$data['childs'] = $this->Childs_model->get_childs();
		$this->load->view('dashboard/childs/index', $data);
	}

	// application/controllers/Childs.php

	public function create()
	{
		$this->load->view('dashboard/childs/create');
	}

	public function store()
	{

		$this->form_validation->set_rules('Nama', 'Nama', 'required');
		$this->form_validation->set_rules('code', 'Code', 'required');
		$this->form_validation->set_rules('Tanggal_lahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('Berat_badan_lahir', 'Berat Badan Lahir', 'numeric');
		$this->form_validation->set_rules('Panjang_badan_lahir', 'Panjang Badan Lahir', 'numeric');
		$this->form_validation->set_rules('Jenis_kelamin', 'Jenis Kelamin', 'required|in_list[P,L]');

		if ($this->form_validation->run() == FALSE) {
			// Form validation failed, reload the create form with errors
			$this->load->view('dashboard/childs/create');
		} else {
			$data = array(
				'Nama' => $this->input->post('Nama'),
				'id_parent' => $this->input->post('id_parent'),
				'code' => $this->input->post('code'),
				'Tanggal_lahir' => $this->input->post('Tanggal_lahir'),
				'Berat_badan_lahir' => $this->input->post('Berat_badan_lahir'),
				'Panjang_badan_lahir' => $this->input->post('Panjang_badan_lahir'),
				'Jenis_kelamin' => $this->input->post('Jenis_kelamin'),
			);
			try {
				$this->Childs_model->create_child($data);
				$this->session->set_flashdata('success', 'Anak Berhasil ditambah');
			} catch (\Throwable $e) {
				$this->session->set_flashdata('error', 'An error occurred: ' . $e->getMessage());
				redirect('childs/create'); // Redirect back to the create form
			}

			// Redirect to the index page after successful insertion
			redirect('childs');
		}
	}

	public function edit($id)
	{


		$data['child'] = $this->Childs_model->get_child($id);
		$this->load->view('dashboard/childs/edit', $data);
	}

	public function update($id)
	{


		// Form validation rules
		$this->form_validation->set_rules('Nama', 'Nama', 'required');
		$this->form_validation->set_rules('Tanggal_lahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('code', 'Code', 'required');
		$this->form_validation->set_rules('Berat_badan_lahir', 'Berat Badan Lahir', 'numeric');
		$this->form_validation->set_rules('Panjang_badan_lahir', 'Panjang Badan Lahir', 'numeric');
		$this->form_validation->set_rules('Jenis_kelamin', 'Jenis Kelamin', 'required|in_list[P,L]');

		if ($this->form_validation->run() == FALSE) {
			// Form validation failed, reload the edit form with errors
			$data['child'] = $this->Childs_model->get_child($id);
			$this->load->view('dashboard/childs/edit', $data);
		} else {
			// Form validation passed, update data in the database
			$data = array(
				'Nama' => $this->input->post('Nama'),
				'Tanggal_lahir' => $this->input->post('Tanggal_lahir'),
				'id_parent' => $this->input->post('id_parent'),
				'code' => $this->input->post('code'),
				'Berat_badan_lahir' => $this->input->post('Berat_badan_lahir'),
				'Panjang_badan_lahir' => $this->input->post('Panjang_badan_lahir'),
				'Jenis_kelamin' => $this->input->post('Jenis_kelamin'),
			);

			try {
				$this->Childs_model->update_child($id, $data);
				$this->session->set_flashdata('success', 'Anak Berhasil diubah');
			} catch (\Throwable $e) {
				$this->session->set_flashdata('error', 'An error occurred: ' . $e->getMessage());
				redirect('childs/edit'); // Redirect back to the create form
			}


			// Redirect to the index page after successful update
			redirect('childs');
		}
	}

	public function delete($id)
	{
		try {
			$this->Childs_model->delete_child($id);
			$this->session->set_flashdata('success', 'Anak Berhasil dihapus');
		} catch (\Throwable $e) {
			$this->session->set_flashdata('error', 'An error occurred: ' . $e->getMessage());
			redirect('childs'); // Redirect back to the create form
		}
		redirect('childs');
	}

	public function submit_weight()
	{
		$selectedChildId = $this->input->post('selected_child_id');

		if ($selectedChildId) {
			// Ambil data anak berdasarkan ID yang dipilih
			$selectedChild = $this->db->get_where('childs', ['id' => $selectedChildId])->row();

			if ($selectedChild) {
				// Aktifkan anak yang dipilih dengan mengupdate field "is_on" menjadi 1
				$this->db->where('id', $selectedChildId);
				$this->db->update('childs', ['is_on' => 1]);

				// Nonaktifkan anak lainnya dengan mengupdate field "is_on" menjadi 0
				$this->db->where('id !=', $selectedChildId);
				$this->db->update('childs', ['is_on' => 0]);

				// Tambahkan Flashdata untuk notifikasi dengan nama anak
				$this->session->set_flashdata('success-anak',  $selectedChild->Nama . ' telah berhasil diaktifkan.');
			} else {
				// Tambahkan Flashdata jika anak tidak ditemukan
				$this->session->set_flashdata('error-anak', 'Anak tidak ditemukan.');
			}
		}

		redirect('childs'); // Redirect ke halaman utama setelah mengaktifkan anak yang dipilih
	}


	public function child_off_all()
	{
		// Mematikan semua anak dengan mengupdate field "is_on" menjadi 0
		$this->db->update('childs', ['is_on' => 0]);

		redirect('childs'); // Redirect ke halaman utama setelah mematikan semua anak
	}



	public function growth($child_id)
	{
		// Mengambil data pertumbuhan anak berdasarkan ID anak
		$this->db->where('id_child', $child_id);
		$data['growth_data'] = $this->db->get('histories')->result();


		$this->db->select('p.*, u.*'); // Use 'p' for parents and 'u' for users
		$this->db->from('childs AS c');
		$this->db->join('parents AS p', 'c.id_parent = p.id', 'left');
		$this->db->join('users AS u', 'p.id_user = u.id', 'left');
		$this->db->where('c.id', $child_id);

		$data['parent_user_data'] = $this->db->get()->row();

		// Mengambil data anak berdasarkan ID anak
		$this->db->where('id', $child_id);

		$data['child_data'] = $this->db->get('childs')->row();


		// Load view dan kirimkan data pertumbuhan dan data anak ke view
		$this->load->view('dashboard/childs/growth', $data);
	}

	public function height_graph($child_id)
	{
		$this->db->where('id_child', $child_id);
		$data['growth_data'] = $this->db->get('histories')->result();

		// Initialize arrays for Panjang and Berat
		$panjang_data = [];

		// Initialize the categories array
		$categories = [];

		// Create an array to hold data for each of the 60 months
		for ($i = 1; $i <= 60; $i++) {
			$categories[] =  $i;
			$panjang_data[] = null;
		}

		// Match the "Panjang" and "Berat" data with the appropriate age/month
		foreach ($data['growth_data'] as $entry) {
			$usia = (int)$entry->usia; // Convert usia to an integer
			if ($usia <= 60) {
				// If usia is within 60 months, update the corresponding data
				$panjang_data[$usia - 1] = (float)$entry->panjang; // Convert panjang to a float
			}
		}

		// Pass the filtered data to the view
		$data['categories'] = $categories;
		$data['panjang_data'] = $panjang_data;

		$child = $this->db->get_where('childs', array('id' => $child_id))->row();

		$height_nutrients = $this->db->get('height_nutrients')->result();
		$gender = $child->Jenis_kelamin;

		// Initialize arrays for normal data
		$normal_awal = [];
		$normal_akhir = [];


		foreach ($height_nutrients as $entry) {
			if ($gender === 'L') {
				$normal_awal[] = (int)$entry->Tinggi_awal_laki;
				$normal_akhir[] = (int)$entry->Tinggi_akhir_laki;
			} elseif ($gender === 'P') {
				$normal_awal[] = (int)$entry->Tinggi_awal_perempuan;
				$normal_akhir[] = (int)$entry->Tinggi_akhir_perempuan;
			}
		}

		$data['normal_awal'] = $normal_awal;
		$data['normal_akhir'] = $normal_akhir;

		$this->load->view('dashboard/childs/height_graph', $data);
	}

	public function weight_graph($child_id)
	{
		$this->db->where('id_child', $child_id);
		$data['growth_data'] = $this->db->get('histories')->result();

		// Initialize arrays for berat and Berat
		$berat_data = [];

		// Initialize the categories array
		$categories = [];

		// Create an array to hold data for each of the 60 months
		for ($i = 1; $i <= 60; $i++) {
			$categories[] =  $i;
			$berat_data[] = null;
		}

		// Match the "berat" and "Berat" data with the appropriate age/month
		foreach ($data['growth_data'] as $entry) {
			$usia = (int)$entry->usia; // Convert usia to an integer
			if ($usia <= 60) {
				// If usia is within 60 months, update the corresponding data
				$berat_data[$usia - 1] = (float)$entry->berat; // Convert berat to a float
			}
		}

		// Pass the filtered data to the view
		$data['categories'] = $categories;
		$data['berat_data'] = $berat_data;

		$child = $this->db->get_where('childs', array('id' => $child_id))->row();

		$weight_nutrients = $this->db->get('weight_nutrients')->result();
		$gender = $child->Jenis_kelamin;

		// In controller

		// Initialize arrays for normal data
		$normal_awal = [];
		$normal_akhir = [];


		foreach ($weight_nutrients as $entry) {
			if ($gender === 'L') {
				$normal_awal[] = (int)$entry->Berat_awal_laki;
				$normal_akhir[] = (int)$entry->Berat_akhir_laki;
			} elseif ($gender === 'P') {
				$normal_awal[] = (int)$entry->Berat_awal_perempuan;
				$normal_akhir[] = (int)$entry->Berat_akhir_perempuan;
			}
		}

		$data['normal_awal'] = $normal_awal;
		$data['normal_akhir'] = $normal_akhir;

		$this->load->view('dashboard/childs/weight_graph', $data);
	}
	public function ideal_graph($child_id)
	{
		$this->db->where('id_child', $child_id);
		$data['growth_data'] = $this->db->get('histories')->result();

		$this->load->view('dashboard/childs/ideal_graph', $data);
	}
}
