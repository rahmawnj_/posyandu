<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public function index()
	{
		$this->load->library('auth');
		$this->auth->checkLogin();

		$this->db->select('*');
		$this->db->join('childs', 'childs.id = histories.id_child');
		$histories = $this->db->get('histories')->result();
		$totalWeight = 0;
		$totalHeight = 0;
		$totalBMI = 0;
		$totalChildren = count($histories);

		foreach ($histories as $history) {
			$totalWeight += $history->berat;
			$totalHeight += $history->panjang;
			// Hitung BMI sesuai dengan rumus yang sesuai
			$totalBMI += ($history->berat / (($history->panjang / 100) * ($history->panjang / 100)));
		}

if ($totalChildren > 0) {
    $averageWeight = $totalWeight / $totalChildren;
    $averageHeight = $totalHeight / $totalChildren;
    $averageBMI = $totalBMI / $totalChildren;
} else {
    // Handle the case where there are no records in $histories (totalChildren is zero)
    // You can set these averages to a default value or handle it as needed.
    $averageWeight = 0;
    $averageHeight = 0;
    $averageBMI = 0;
}

	$data = array(
    'averageWeight' => (int)$averageWeight,
    'averageHeight' => (int)$averageHeight,
    'averageBMI' => (int)$averageBMI,
    'histories' => $histories
);

		$this->load->view('dashboard/index', $data);
	}
}
