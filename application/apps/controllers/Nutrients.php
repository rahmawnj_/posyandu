<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nutrients extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Nutrients_model');
		$this->load->library('form_validation');
		$this->load->library('auth');
	}

	public function weights()
	{
		$this->auth->checkLogin();
		$this->auth->isAdmin();

		$data['nutrients'] = $this->db->get('weight_nutrients')->result();

		$this->load->view('dashboard/nutrients/weight', $data);
	}
	
	public function ideals()
	{
		$this->auth->checkLogin();
		$this->auth->isAdmin();
		$this->db->from('ideal_nutrients');
		$this->db->order_by('Tinggi', 'ASC'); // Mengurutkan berdasarkan kolom "Tinggi" dari yang terkecil (ascending)
		$data['nutrients'] = $this->db->get()->result();
		

		$this->load->view('dashboard/nutrients/ideal', $data);
	}

	public function weight_girl_graph()
	{
		$data['nutrients'] = json_decode(json_encode($this->db->get('weight_nutrients')->result()), true);

		$this->load->view('dashboard/nutrients/weight_girl_graph', $data);
	}

	public function weight_boy_graph()
	{
		$data['nutrients'] = json_decode(json_encode($this->db->get('weight_nutrients')->result()), true);

		$this->load->view('dashboard/nutrients/weight_boy_graph', $data);
	}


	public function heights()
	{
		$this->auth->checkLogin();
		$this->auth->isAdmin();
		$data['nutrients'] = $this->db->get('height_nutrients')->result();

		$this->load->view('dashboard/nutrients/height', $data);
	}


	public function ideal_store()
	{
		// Ambil data dari POST
		$tinggi = $this->input->post('Tinggi');
		$beratAwalLaki = $this->input->post('Berat_awal_laki');
		$beratAkhirLaki = $this->input->post('Berat_akhir_laki');
		$keteranganKurusLaki = $this->input->post('Keterangan_Kurus_laki');
		$keteranganNormalLaki = $this->input->post('Keterangan_Normal_laki');
		$keteranganGendutLaki = $this->input->post('Keterangan_Gendut_laki');
		$beratAwalPerempuan = $this->input->post('Berat_awal_perempuan');
		$beratAkhirPerempuan = $this->input->post('Berat_akhir_perempuan');
		$keteranganKurusPerempuan = $this->input->post('Keterangan_Kurus_perempuan');
		$keteranganNormalPerempuan = $this->input->post('Keterangan_Normal_perempuan');
		$keteranganGendutPerempuan = $this->input->post('Keterangan_Gendut_perempuan');

		// Siapkan data untuk disimpan
		$data = [
			'Tinggi' => $tinggi,
			'Berat_awal_laki' => $beratAwalLaki,
			'Berat_akhir_laki' => $beratAkhirLaki,
			'Keterangan_Kurus_laki' => $keteranganKurusLaki,
			'Keterangan_Normal_laki' => $keteranganNormalLaki,
			'Keterangan_Gendut_laki' => $keteranganGendutLaki,
			'Berat_awal_perempuan' => $beratAwalPerempuan,
			'Berat_akhir_perempuan' => $beratAkhirPerempuan,
			'Keterangan_Kurus_perempuan' => $keteranganKurusPerempuan,
			'Keterangan_Normal_perempuan' => $keteranganNormalPerempuan,
			'Keterangan_Gendut_perempuan' => $keteranganGendutPerempuan,
		];

		// Simpan data ke dalam database
		$this->db->insert('ideal_nutrients', $data);

		// Kirim respons
		$this->ideals();
	}
	
		public function ideal_edit()
	{
		$id = $this->input->post('pk'); // Ambil ID yang akan diperbarui
		$field_name = $this->input->post('name'); // Nama kolom yang akan diperbarui
		$new_value = $this->input->post('value'); // Nilai baru

		// Simpan data yang diperbarui ke dalam database
		$data = [
			$field_name => $new_value
		];

		$this->db->where('id', $id);
		$this->db->update('ideal_nutrients', $data);

		// Kirim respons
		echo json_encode(['success' => true]);
	}


	public function weight_store()
	{
		$id = $this->input->post('pk'); // Ambil ID yang akan diperbarui
		$field_name = $this->input->post('name'); // Nama kolom yang akan diperbarui
		$new_value = $this->input->post('value'); // Nilai baru

		// Simpan data yang diperbarui ke dalam database
		$data = [
			$field_name => $new_value
		];

		$this->db->where('id', $id);
		$this->db->update('weight_nutrients', $data);

		// Kirim respons
		echo json_encode(['success' => true]);
	}

	public function height_store()
	{
		$id = $this->input->post('pk'); // Ambil ID yang akan diperbarui
		$field_name = $this->input->post('name'); // Nama kolom yang akan diperbarui
		$new_value = $this->input->post('value'); // Nilai baru

		// Simpan data yang diperbarui ke dalam database
		$data = [
			$field_name => $new_value
		];

		$this->db->where('id', $id);
		$this->db->update('height_nutrients', $data);

		// Kirim respons
		echo json_encode(['success' => true]);
	}

	public function height_girl_graph()
	{
		$data['nutrients'] = json_decode(json_encode($this->db->get('height_nutrients')->result()), true);

		$this->load->view('dashboard/nutrients/height_girl_graph', $data);
	}

	public function height_boy_graph()
	{
		$data['nutrients'] = json_decode(json_encode($this->db->get('height_nutrients')->result()), true);

		$this->load->view('dashboard/nutrients/height_boy_graph', $data);
	}

	public function ideal_delete($id)
	{
		// Ambil ID data yang akan dihapus
		$id = intval($id);

		// Lakukan validasi ID atau kebijakan keamanan lainnya jika diperlukan

		// Hapus data dari tabel 'ideal_nutrients' berdasarkan ID
		$this->db->where('id', $id);
		$this->db->delete('ideal_nutrients');

		// Redirect kembali ke halaman setelah penghapusan data berhasil
		redirect('nutrients/ideals');
	}
}
