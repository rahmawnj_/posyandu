<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("form_validation");
		$this->load->model('Users_model');
		$this->load->library('auth');
		$this->auth->checkLogin();
		$this->auth->isAdmin();
	}

	public function index()
	{
		$role = $this->input->get('role'); // Ambil parameter 'role' dari URL
		$data['users'] = $this->Users_model->get_users($role);
		$this->load->view('dashboard/users/index', $data);
	}

	public function create()
	{
		$this->load->view('dashboard/users/create');
	}

	public function store()
	{
		// Form validation rules
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE) {
			// Form validation failed, reload the create form with errors
			$this->load->view('dashboard/users/create');
		} else {
			// Handle file upload
			$config['upload_path'] = './assets/upload/image/users/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size'] = 2048; // 2MB max file size

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				$upload_data = $this->upload->data();
				$foto = $upload_data['file_name'];

				// Form validation passed, insert data into the database
				$data = array(
					'nama' => $this->input->post('nama'),
					'email' => $this->input->post('email'),
					'role' => $this->input->post('role'),
					'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
					'foto' => $foto
				);

				try {
					$this->Users_model->create_user($data);
					$this->session->set_flashdata('success', 'User Berhasil ditambah');
				} catch (\Throwable $e) {
					$this->session->set_flashdata('error', 'An error occurred: ' . $e->getMessage());
					redirect('users/create'); // Redirect back to the create form
				}

				// Redirect to the index page after successful insertion
				redirect('users');
			} else {
				// File upload failed, reload the create form with errors
				$error = array('error' => $this->upload->display_errors());
				$this->load->view('dashboard/users/create', $error);
			}
		}
	}

	public function edit($id)
	{
		$data['user'] = $this->Users_model->get_user($id);
		$this->load->view('dashboard/users/edit', $data);
	}

	public function update($id)
	{
		// Form validation rules
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');

		if ($this->form_validation->run() == FALSE) {
			// Form validation failed, reload the edit form with errors
			$data['user'] = $this->Users_model->get_user($id);
			$this->load->view('dashboard/users/edit', $data);
		} else {
			// Handle file upload
			$config['upload_path'] = './assets/upload/image/users/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size'] = 2048; // 2MB max file size

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				// Delete previous image file if exists
				$user = $this->Users_model->get_user($id);
				if ($user->foto && file_exists('./assets/upload/image/users/' . $user->foto)) {
					unlink('./assets/upload/image/users/' . $user->foto);
				}

				$upload_data = $this->upload->data();
				$foto = $upload_data['file_name'];
			} else {
				// Keep the existing image if file upload fails
				$user = $this->Users_model->get_user($id);
				$foto = $user->foto;
			}

			// Form validation passed, update data in the database
			$data = array(
				'nama' => $this->input->post('nama'),
				'email' => $this->input->post('email'),
				'role' => $this->input->post('role'),
				'foto' => $foto
			);

			try {
				$this->Users_model->update_user($id, $data);
				$this->session->set_flashdata('success', 'User Berhasil diubah');
			} catch (\Throwable $e) {
				$this->session->set_flashdata('error', 'An error occurred: ' . $e->getMessage());
				redirect('users/edit'); // Redirect back to the create form
			}

			// Redirect to the index page after successful update
			redirect('users');
		}
	}

	public function delete($id)
	{
		// Delete image file before deleting the user record
		$user = $this->Users_model->get_user($id);
		if ($user->foto && file_exists('./assets/upload/image/users/' . $user->foto)) {
			unlink('./assets/upload/image/users/' . $user->foto);
		}

		try {
			$this->Users_model->delete_user($id);
			$this->session->set_flashdata('success', 'User Berhasil dihapus');
		} catch (\Throwable $e) {
			$this->session->set_flashdata('error', 'An error occurred: ' . $e->getMessage());
			redirect('users/edit'); // Redirect back to the create form
		}		

		redirect('users');
	}
}
