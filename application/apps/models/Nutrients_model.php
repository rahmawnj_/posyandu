<?php
class Nutrients_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function get_nutrients()
    {
        $query = $this->db->get('nutrients');
        return $query->result();
    }
    
    public function get_nutrient($id)
    {
        $query = $this->db->get_where('nutrients', array('id' => $id));
        return $query->row();
    }
    
    public function create_nutrient($data)
    {
        $this->db->insert('nutrients', $data);
        return $this->db->insert_id();
    }
    
    public function update_nutrient($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('nutrients', $data);
    }
    
    public function delete_nutrient($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('nutrients');
    }
}
?>
