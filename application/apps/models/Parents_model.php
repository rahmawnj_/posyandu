<?php

class Parents_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_parents()
	{
		$this->db->select('parents.*, users.nama AS user_nama, COUNT(childs.id) AS child_count');
		$this->db->join('users', 'parents.id_user = users.id', 'left');
		$this->db->join('childs', 'parents.id = childs.id_parent', 'left');
		$this->db->group_by('parents.id'); // Mengelompokkan hasil berdasarkan id orang tua
		$query = $this->db->get('parents');
	
		return $query->result();
	}

	public function get_parent($id)
	{
		$query = $this->db->get_where('parents', array('id' => $id));
		return $query->row();
	}

	public function create_parent($data)
	{
		$this->db->insert('parents', $data);
		return $this->db->insert_id();
	}

	public function update_parent($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('parents', $data);
	}

	public function delete_parent($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('parents');
	}
}
