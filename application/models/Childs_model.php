<?php
class Childs_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_childs()
    {
        $role = $this->session->userdata('role'); // Dapatkan peran (role) pengguna dari sesi

        if ($role === 'Admin') {
            // $this->db->select('childs.*, parents.Nama_ibu as Nama_ibu');
            $this->db->from('childs');
            // $this->db->join('parents', 'childs.id_parent = parents.id', 'left'); // Use a left join to include children without parents
            $query = $this->db->get();
        } elseif ($role === 'Parent') {
            $query = $this->db->get_where('childs', ['id_parent' => $this->session->userdata('parent_id')]);
        } else {
            // Handle peran lainnya sesuai kebutuhan
            $query = $this->db->get('childs'); // Contoh: ambil semua data anak jika peran tidak valid
        }

        return $query->result();
    }

    public function get_child($id)
    {
        $query = $this->db->get_where('childs', array('id' => $id));
        return $query->row();
    }

    public function create_child($data)
    {
        $this->db->insert('childs', $data);
        return $this->db->insert_id();
    }

    public function update_child($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('childs', $data);
    }

    public function delete_child($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('childs');
    }
}
