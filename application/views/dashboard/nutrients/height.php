<?php
$title = 'Gizi';
$breadcrumb = ['Master Gizi', 'Tinggi'];

$css_urls = [
	base_url('assets/d/plugins/x-editable-bs4/dist/bootstrap4-editable/css/bootstrap-editable.css'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/address/address.css'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css'),
	base_url('assets/d/plugins/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css'),
	base_url('assets/d/plugins/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css'),
	base_url('assets/d/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'),
	base_url('assets/d/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css'),
	base_url('assets/d/plugins/select2/dist/css/select2.min.css'),
];

$js_urls = [
	base_url('https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js'),
	base_url('assets/d/plugins/jquery-migrate/dist/jquery-migrate.min.js'),
	base_url('assets/d/plugins/x-editable-bs4/dist/bootstrap4-editable/js/bootstrap-editable.min.js'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/address/address.js'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/typeaheadjs/lib/typeahead.js'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/typeaheadjs/typeaheadjs.js'),
	base_url('assets/d/plugins/x-editable-bs4/dist/inputs-ext/wysihtml5/wysihtml5.js'),
	base_url('assets/d/plugins/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js'),
	base_url('assets/d/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'),
	base_url('assets/d/plugins/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js'),
	base_url('assets/d/plugins/select2/dist/js/select2.full.min.js'),
	base_url('assets/d/plugins/jquery-mockjax/dist/jquery.mockjax.min.js'),
	base_url('assets/d/plugins/moment/min/moment.min.js'),
];

ob_start();
?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="table-responsive">
		<table id="user" class="table table-bordered table-panel mb-0">
			<thead class="bg-info text-dark">
				<tr>
					<th width="2%">Umur (Bulan)</th>
					<th colspan="3" class="text-center">Laki-Laki</th>
					<th colspan="3" class="text-center">Perempuan</th>
				</tr>
				<tr class="text-center">
					<th></th>
					<th>Pendek (cm) &le; </th>
					<th>Normal (cm)</th>
					<th>&ge; Tinggi (cm)</th>
					<th>Pendek (cm) &le;</th>
					<th>Normal (cm)</th>
					<th>&ge; Tinggi (cm)</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($nutrients as $key => $nutrion) : ?>
					<tr>
						<td rowspan="3" class="bg-light"><?= $nutrion->Usia ?></td>
						<td>
							<span id="PendekLaki<?= $nutrion->id ?>"></span> <!-- Menampilkan nilai "Pendek" Laki-Laki -->
						</td>
						<td>
							<a href="#" class="editable" data-type="number" data-pk="<?= $nutrion->id ?>" data-name="Tinggi_awal_laki" data-title="Normal (Tinggi Awal)"><?= $nutrion->Tinggi_awal_laki ?></a> -
							<a href="#" class="editable" data-type="number" data-pk="<?= $nutrion->id ?>" data-name="Tinggi_akhir_laki" data-title="Normal (Tinggi Akhir)"><?= $nutrion->Tinggi_akhir_laki ?></a>
						</td>
						<td>
							<span id="TinggiLaki<?= $nutrion->id ?>"></span> <!-- Menampilkan hasil perhitungan "Tinggi" Laki-Laki -->
						</td>
						<td>
							<span id="PendekPerempuan<?= $nutrion->id ?>"></span> <!-- Menampilkan nilai "Pendek" Perempuan -->
						</td>
						<td>
							<a href="#" class="editable" data-type="number" data-pk="<?= $nutrion->id ?>" data-name="Tinggi_awal_perempuan" data-title="Normal (Tinggi Awal)"><?= $nutrion->Tinggi_awal_perempuan ?></a> -
							<a href="#" class="editable" data-type="number" data-pk="<?= $nutrion->id ?>" data-name="Tinggi_akhir_perempuan" data-title="Normal (Tinggi Akhir)"><?= $nutrion->Tinggi_akhir_perempuan ?></a>
						</td>
						<td>
							<span id="TinggiPerempuan<?= $nutrion->id ?>"></span> <!-- Menampilkan hasil perhitungan "Tinggi" Perempuan -->
						</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Pendek_laki" data-title="Keterangan"><?= $nutrion->Keterangan_Pendek_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Normal_laki" data-title="Keterangan"><?= $nutrion->Keterangan_Normal_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Tinggi_laki" data-title="Keterangan"><?= $nutrion->Keterangan_Tinggi_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Pendek_perempuan" data-title="Keterangan"><?= $nutrion->Keterangan_Pendek_perempuan ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Normal_perempuan" data-title="Keterangan"><?= $nutrion->Keterangan_Normal_perempuan ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="text" data-pk="<?= $nutrion->id ?>" data-name="Keterangan_Tinggi_perempuan" data-title="Keterangan"><?= $nutrion->Keterangan_Tinggi_perempuan ?></a>
						</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Pendek_laki" data-title="Gizi"><?= $nutrion->Gizi_Pendek_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Normal_laki" data-title="Gizi"><?= $nutrion->Gizi_Normal_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Tinggi_laki" data-title="Gizi"><?= $nutrion->Gizi_Tinggi_laki ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Pendek_perempuan" data-title="Gizi"><?= $nutrion->Gizi_Pendek_perempuan ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Normal_perempuan" data-title="Gizi"><?= $nutrion->Gizi_Normal_perempuan ?></a>
						</td>
						<td>
							<a href="#" class="editable" data-type="textarea" data-pk="<?= $nutrion->id ?>" data-name="Gizi_Tinggi_perempuan" data-title="Gizi"><?= $nutrion->Gizi_Tinggi_perempuan ?></a>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>

<!-- JavaScript -->
<script>
	$(document).ready(function() {
		// Fungsi untuk menghitung nilai "Pendek" dan "Tinggi" dan mengisi selnya
		function calculateAndFillValues() {
			<?php foreach ($nutrients as $key => $nutrion) : ?>
				// Ambil nilai "Normal (Tinggi Awal)" dan "Normal (Tinggi Akhir)" dari elemen saat halaman dimuat
				var TinggiAwalLaki<?= $nutrion->id ?> = <?= $nutrion->Tinggi_awal_laki ?>;
				var TinggiAkhirLaki<?= $nutrion->id ?> = <?= $nutrion->Tinggi_akhir_laki ?>;
				var TinggiAwalPerempuan<?= $nutrion->id ?> = <?= $nutrion->Tinggi_awal_perempuan ?>;
				var TinggiAkhirPerempuan<?= $nutrion->id ?> = <?= $nutrion->Tinggi_akhir_perempuan ?>;

				// Hitung "Pendek" berdasarkan nilai "Normal (Tinggi Awal)"
				var PendekLaki<?= $nutrion->id ?> = TinggiAwalLaki<?= $nutrion->id ?> - 1;
				var PendekPerempuan<?= $nutrion->id ?> = TinggiAwalPerempuan<?= $nutrion->id ?> - 1;

				// Hitung "Tinggi" berdasarkan nilai "Normal (Tinggi Akhir)"
				var TinggiLaki<?= $nutrion->id ?> = TinggiAkhirLaki<?= $nutrion->id ?> + 1;
				var TinggiPerempuan<?= $nutrion->id ?> = TinggiAkhirPerempuan<?= $nutrion->id ?> + 1;

				// Isi sel "Pendek" dengan nilai yang dihitung
				document.getElementById("PendekLaki<?= $nutrion->id ?>").textContent = PendekLaki<?= $nutrion->id ?>;
				document.getElementById("PendekPerempuan<?= $nutrion->id ?>").textContent = PendekPerempuan<?= $nutrion->id ?>;

				// Isi sel "Tinggi" dengan hasil perhitungan
				document.getElementById("TinggiLaki<?= $nutrion->id ?>").textContent =  TinggiLaki<?= $nutrion->id ?>;
				document.getElementById("TinggiPerempuan<?= $nutrion->id ?>").textContent =  TinggiPerempuan<?= $nutrion->id ?>;
			<?php endforeach ?>
		}

		// Panggil fungsi saat halaman dimuat
		calculateAndFillValues();

		// Aktifkan Editable Bootstrap
		$('.editable').editable({
			mode: 'inline',
			inputclass: 'form-control input-sm',
			url: '<?= base_url('nutrients/height_store') ?>' // Ganti dengan URL yang sesuai untuk menyimpan data ke server
		});


	});
</script>
