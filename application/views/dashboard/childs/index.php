<?php
$title = 'Master Anak';
$breadcrumb = ['Master Anak'];

$css_urls = [
	base_url('assets/d/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css'),
	base_url('assets/d/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css'),
	base_url('assets/d/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css'),
	base_url('assets/d/plugins/gritter/css/jquery.gritter.css'),
];



$js_urls = [
	base_url('assets/d/plugins/datatables.net/js/jquery.dataTables.min.js'),
	base_url('assets/d/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js'),
	base_url('assets/d/plugins/datatables.net-responsive/js/dataTables.responsive.min.js'),
	base_url('assets/d/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/dataTables.buttons.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.colVis.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.flash.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.html5.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.print.min.js'),
	base_url('assets/d/plugins/pdfmake/build/pdfmake.min.js'),
	base_url('assets/d/plugins/pdfmake/build/vfs_fonts.js'),
	base_url('assets/d/plugins/jszip/dist/jszip.min.js'),
	base_url('assets/d/plugins/gritter/js/jquery.gritter.js'),
	base_url('assets/d/plugins/sweetalert/dist/sweetalert.min.js'),
	base_url('assets/d/js/demo/table-manage-buttons.demo.js'),
	base_url('assets/d/plugins/@highlightjs/cdn-assets/highlight.min.js'),
	base_url('assets/d/js/demo/render.highlight.js'),
];


ob_start();

?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?>
		</h4>
		<div class="panel-heading-btn">
			<?php if ($this->session->userdata('role') == 'Admin') : ?>
				<a href="<?= base_url('childs/child_off_all') ?>" class="btn btn-xs btn-indigo"><i class="fa fa-minus"></i> </a>
			<?php endif ?>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
			<?php if ($this->session->userdata('role') == 'Admin') : ?>
				<a href="<?= base_url('childs/create') ?>" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
			<?php endif ?>

		</div>
	</div>
	<div class="panel-body">
		<?php if ($this->session->userdata('role') == 'Admin') : ?>
			<form method="post" action="<?= site_url('childs/submit_weight'); ?>">
				<table id="data-table" class="table table-striped table-bordered align-middle">
					<thead>
						<tr>
							<th width="1%"></th>
							<th>Nama</th>
							<th>Kode</th>
							<th>Tanggal Lahir</th>
							<th>Umur</th>
							<th>Aktif</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($childs as $key => $child) : ?>
							<tr>
								<td><?= $key + 1 ?></td>
								<td><?= $child->Nama; ?></td>
								<td><?= $child->code; ?></td>
								<td><?= $child->Tanggal_lahir; ?></td>
								<td>
									<?php
									$tanggal_lahir = new DateTime($child->Tanggal_lahir);
									$sekarang = new DateTime('now');

									if ($sekarang > $tanggal_lahir) {
										$perbedaan = $sekarang->diff($tanggal_lahir);
										$umur_bulan = $perbedaan->y * 12 + $perbedaan->m;
										echo $umur_bulan . ' bulan';
									} else {
										echo '0 bulan';
									}
									?>
								</td>
								<td>
									<?php if ($umur_bulan <= 60) : ?>
										<form method="post" action="<?= site_url('childs/submit_weight'); ?>">
											<input type="radio" name="selected_child_id" value="<?= $child->id; ?>" <?= ($child->is_on == 1) ? 'checked' : ''; ?> onchange="this.form.submit()">
										</form>
									<?php endif ?>
								</td>
								<td>
									<a href="<?= site_url('childs/growth/' . $child->id); ?>" class="btn btn-indigo btn-sm"><i class="fas fa-neuter"></i></a>
									<a href="<?= site_url('childs/height_graph/' . $child->id); ?>" class="btn btn-warning btn-sm" title="Tinggi Anak"><i class="fas fa-chart-line"></i></a>
									<a href="<?= site_url('childs/weight_graph/' . $child->id); ?>" class="btn btn-danger btn-sm" title="Berat Anak"><i class="fas fa-chart-line"></i></a>
									<a href="<?= site_url('childs/ideal_graph/' . $child->id); ?>" class="btn btn-success btn-sm" title="Tinggi x Berat"><i class="fas fa-chart-line"></i></a>
									<a href="<?= site_url('childs/edit/' . $child->id); ?>" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
									<a href="<?= site_url('childs/delete/' . $child->id); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this child?')"><i class="fas fa-trash-alt"></i></a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</form>
		<?php else : ?>
			<table id="data-table" class="table table-striped table-bordered align-middle">
				<thead>
					<tr>
						<th width="1%"></th>
						<th>Nama</th>
						<th>Kode</th>
						<th>Tanggal Lahir</th>
						<th>Umur</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($childs as $key => $child) : ?>
						<tr>
							<td><?= $key + 1 ?></td>
							<td><?= $child->Nama; ?></td>
							<td><?= $child->code; ?></td>
							<td><?= $child->Tanggal_lahir; ?></td>
							<td>
								<?php
								$tanggal_lahir = new DateTime($child->Tanggal_lahir);
								$sekarang = new DateTime('now');

								if ($sekarang > $tanggal_lahir) {
									$perbedaan = $sekarang->diff($tanggal_lahir);
									$umur_bulan = $perbedaan->y * 12 + $perbedaan->m;
									echo $umur_bulan . ' bulan';
								} else {
									echo '0 bulan';
								}
								?>
							</td>
							<td><a href="<?= site_url('childs/growth/' . $child->id); ?>" class="btn btn-indigo btn-sm"><i class="fas fa-neuter"></i></a>
								<a href="<?= site_url('childs/height_graph/' . $child->id); ?>" class="btn btn-warning btn-sm" title="Tinggi Anak"><i class="fas fa-chart-line"></i></a>
								<a href="<?= site_url('childs/weight_graph/' . $child->id); ?>" class="btn btn-danger btn-sm" title="Berat Anak"><i class="fas fa-chart-line"></i></a>
								<a href="<?= site_url('childs/ideal_graph/' . $child->id); ?>" class="btn btn-success btn-sm" title="Tinggi x Berat"><i class="fas fa-chart-line"></i></a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif ?>
	</div>
</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>

<script>
	$(document).ready(function() {

		var dataTable = $('#data-table').DataTable({
			// Add any DataTable configuration options you need
			dom: 'Bfrtip', // Add DataTables Buttons to the DOM
			buttons: [ 
				'copy','print'			]
		});
		<?php if ($this->session->flashdata('success-anak')) : ?>
			$.gritter.add({
				title: 'Success',
				text: '<?= $this->session->flashdata('success-anak') ?>',
				image: '',
				class_name: 'gritter-success'
			});
		<?php endif; ?>
	});
</script>
<script>
	$(document).ready(function() {
		<?php if ($this->session->flashdata('error-anak')) : ?>
			$.gritter.add({
				title: 'error',
				text: '<?= $this->session->flashdata('error-anak') ?>',
				image: '',
				class_name: 'gritter-error'
			});
		<?php endif; ?>
	});
</script>
<?php if ($this->session->flashdata('success')) : ?>
	<script>
		swal({
			title: 'Success',
			text: '<?= $this->session->flashdata('success') ?>',
			icon: 'success',
			buttons: {
				confirm: {
					text: 'OK',
					className: 'btn btn-primary'
				}
			}
		});
	</script>
<?php endif; ?>
<?php if ($this->session->flashdata('error')) : ?>
	<script>
		swal({
			title: 'error',
			text: '<?= $this->session->flashdata('error') ?>',
			icon: 'error',
			buttons: {
				confirm: {
					text: 'OK',
					className: 'btn btn-primary'
				}
			}
		});
	</script>
<?php endif; ?>