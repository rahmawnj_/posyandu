<?php
$title = 'Master Anak';
$breadcrumb = ['Master Anak', 'Tambah Anak'];

$css_urls = [];

$js_urls = [base_url('assets/d/plugins/apexcharts/dist/apexcharts.min.js')];

ob_start();

?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
		<div id="apex-column-chart"></div>

	</div>


</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>

<script>
	var chart = new ApexCharts(
		document.querySelector('#apex-column-chart'), {
			chart: {
				height: 350,
				type: 'bar'
			},
			title: {
				text: 'Berat & Panjang Grafik',
				align: 'center'
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'rounded'
				}
			},
			dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			colors: [COLOR_DARK, COLOR_INDIGO, COLOR_SILVER],
			series: [
				{
					name: 'Berat',
					data: <?= json_encode($berat_data) ?>
				},{
					name: 'Panjang',
					data: <?= json_encode($panjang_data) ?>
				},
				
			],
			xaxis: {
				categories: <?= json_encode($categories) ?>,
				axisBorder: {
					show: true,
					color: COLOR_SILVER_TRANSPARENT_5,
					height: 1,
					width: '100%',
					offsetX: 0,
					offsetY: -1
				},
				axisTicks: {
					show: true,
					borderType: 'solid',
					color: COLOR_SILVER,
					height: 6,
					offsetX: 0,
					offsetY: 0
				}
			},
			yaxis: {
				title: {
					text: 'Panjang & Berat'
				}
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function(val) {
						return val
					}
				}
			}
		}
	);
	chart.render();
</script>
