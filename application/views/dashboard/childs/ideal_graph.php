<?php
$title = 'Master Anak';
$breadcrumb = ['Master Anak', 'Tambah Anak'];

$css_urls = [];

$js_urls = [base_url('assets/d/plugins/apexcharts/dist/apexcharts.min.js')];

ob_start();

?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
		<div id="chart"></div>
	</div>


</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>

<?php

// Buat array untuk data panjang dan berat dari data yang diberikan
$panjang = array();
$berat = array();

foreach ($growth_data as $data) {
	$panjang[] = (float)$data->panjang;
	$berat[] = (float)$data->berat;
}
?>

<script>
	document.addEventListener("DOMContentLoaded", function() {
		var options = {
			chart: {
				type: "line",
				height: 350,
			},
			series: [{
				name: "Berat (kg)",
				data: <?= json_encode($berat) ?>
			}],
			xaxis: {
				categories: <?= json_encode($panjang) ?>,
				title: {
					text: "Tinggi (cm)" // Ganti teks dengan judul yang Anda inginkan
				},
			},
			yaxis: {
				title: {
					text: "Berat (kg)" // Ganti teks dengan judul yang Anda inginkan
				},
			},
		};

		var chart = new ApexCharts(document.querySelector("#chart"), options);
		chart.render();
	});
</script>