<?php
$title = 'Master Anak';
$breadcrumb = ['Master Anak', 'Riwayat Tumbuh Kembang'];

$css_urls = [];

$js_urls = [];

// SCRIPT
function getWeightKeterangan($result, $usia)
{
	global $weight_nutrients; // Anda perlu mendefinisikan variabel ini dari hasil query ke tabel `weight_nutrients`

	foreach ($weight_nutrients as $nutrient) {
		if ((int)$nutrient->Usia == $usia) {
			if ($result == '<') {
				return $nutrient->Keterangan_Kurus_laki;
			} elseif ($result == '=') {
				return $nutrient->Keterangan_Normal_laki;
			} elseif ($result == '>') {
				return $nutrient->Keterangan_Gendut_laki;
			}
		}
	}
	return '';
}

function getHeightKeterangan($result, $usia)
{
	global $weight_nutrients; // Anda perlu mendefinisikan variabel ini dari hasil query ke tabel `weight_nutrients`

	foreach ($weight_nutrients as $nutrient) {
		if ((int)$nutrient->Usia == $usia) {
			if ($result == '<') {
				return $nutrient->Keterangan_Kurus_perempuan;
			} elseif ($result == '=') {
				return $nutrient->Keterangan_Normal_perempuan;
			} elseif ($result == '>') {
				return $nutrient->Keterangan_Gendut_perempuan;
			}
		}
	}
	return '';
}

// SCRIPT

ob_start();

?>
<style>
	/* Hide everything except the content within the 'invoice' div when printing */
	@media print {
		body * {
			visibility: hidden;
		}

		.invoice,
		.invoice * {
			visibility: visible;
		}

		.invoice {
			position: absolute;
			left: 0;
			top: 0;
		}

		#print-button {
			display: none;
		}
	}
</style>
<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="invoice">
	<!-- BEGIN invoice-company -->
	<div class="invoice-company">
		<span class="float-end hidden-print" id="print-button">
			<a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-white mb-10px"><i class="fa fa-print t-plus-1 fa-fw fa-lg"></i> Print</a>
		</span>
		<?= $this->db->get_where('settings', ['name' => 'app_name'])->row()->value ?>
	</div>
	<!-- END invoice-company -->
	<!-- BEGIN invoice-header -->
	<div class="invoice-header">
		<div class="invoice-from">
			<small>Data Anak</small>
			<address class="mt-5px mb-5px">
				<strong class="text-inverse"> <?= $child_data->Nama ?></strong><br />
				Tanggal Lahir : <?= $child_data->Tanggal_lahir ?><br />
				Usia : <?php
						$tanggal_lahir = new DateTime($child_data->Tanggal_lahir);
						$sekarang = new DateTime('now');

						if ($sekarang > $tanggal_lahir) {
							$perbedaan = $sekarang->diff($tanggal_lahir);
							$umur_bulan = $perbedaan->y * 12 + $perbedaan->m;
							echo $umur_bulan . ' bulan';
						} else {
							echo '0 bulan';
						}
						?><br />
				Panjang Badan Lahir: <?= $child_data->Panjang_badan_lahir ?> cm <br />
				Berat Badan Lahir: <?= $child_data->Berat_badan_lahir ?> kg
			</address>
		</div>
		<div class="invoice-to">
			<small>Orang Tua</small>
			<address class="mt-5px mb-5px">
				<strong class="text-inverse"><?= $parent_user_data->email ?></strong><br />
				Nama Ayah : <?= $parent_user_data->Nama_ayah ?><br />
				Nama Ibu : <?= $parent_user_data->Nama_ibu ?><br />
				Alamat: <?= $parent_user_data->Alamat ?><br />
			</address>
		</div>
		<div class="invoice-date">
			<small>Riwayat / Bulan <?php echo date('F', strtotime('this month')); ?></small>

			<div class="invoice-detail mt-5px">
				<?php
				$usia_array = [];

				// Loop melalui data pertumbuhan dan tambahkan usia ke array
				foreach ($growth_data as $growth) {
					$usia_array[] = (int) $growth->usia;
				}

				// Urutkan usia dalam urutan menaik
				sort($usia_array);

				// Ambil usia yang terakhir
				$usia_terakhir = end($usia_array);

				// Cari data berat pada usia terakhir
				$berat_terakhir = null;
				foreach ($growth_data as $growth) {
					if ((int)$growth->usia === $usia_terakhir) {
						$berat_terakhir = $growth->berat;
						$panjang_terakhir = $growth->panjang ?? 0;
						break;
					}
				}

				// Tampilkan usia dan berat terakhir
				?>
				Berat Terakhir : <?= $berat_terakhir ?? '' ?> <br>
				Panjang Terakhir : <?= $panjang_terakhir ?? '' ?> <br>
			</div>

			<div class="">
				<div class="mt-5px">
					Berat Badan:

				</div>
				<div class="mt-5px">
					Panjang Badan:

				</div>
			</div>




		</div>
	</div>
	<!-- END invoice-header -->
	<!-- BEGIN invoice-content -->
	<div class="invoice-content">
		<!-- BEGIN table-responsive -->
		<div class="table-responsive">
			<table class="table table-invoice">
				<thead>
					<tr>
						<th>Usia (bulan)</th>
						<th class="text-center">Panjang (cm)</th>
						<th class="text-center">Berat (kg)</th>
						<th class="text-center">Hasil Berat</th>
						<th class="text-center">Hasil Panjang</th>
						<th class="text-center">Hasil Berat x Panjang</th>
					</tr>
				</thead>
				<tbody>
					<?php
					for ($usia = 1; $usia <= 60; $usia++) {
						$growth_data_for_usia = null;
						if (!empty($growth_data)) {
							foreach ($growth_data as $growth) {
								if ((int)$growth->usia == $usia) {
									$growth_data_for_usia = $growth;
									break;
								}
							}
						}
					?>
						<tr>
							<td><?= $usia ?></td>
							<td class="text-center"><?= $growth_data_for_usia ? $growth_data_for_usia->panjang : '' ?></td>
							<td class="text-center"><?= $growth_data_for_usia ? $growth_data_for_usia->berat : '' ?></td>
							<td class="text-center text-<?php echo $growth_data_for_usia ? ($growth_data_for_usia->height_result == '<' ? 'warning' : ($growth_data_for_usia->height_result == '>' ? 'danger' : ($growth_data_for_usia->height_result == '=' ? 'black' : ''))) : ''; ?>">
								<?php
								$keterangan = '';
								if (!empty($growth_data_for_usia)) {
									$usia = $growth_data_for_usia->usia;
									$jenis_kelamin = $child_data->Jenis_kelamin; // Assuming you have stored gender in $data
									$this->db->where('Usia', $usia);
									$this->db->select('*');
									$this->db->limit(1);
									$result = $this->db->get('height_nutrients')->row();
									if (!empty($result)) {
										if ($growth_data_for_usia->height_result === '<') {
											if ($jenis_kelamin === 'L') {
												$keterangan = $result->Keterangan_Pendek_laki;
											} elseif ($jenis_kelamin === 'P') {
												$keterangan = $result->Keterangan_Pendek_perempuan;
											}
										} elseif ($growth_data_for_usia->height_result === '=') {
											if ($jenis_kelamin === 'L') {
												$keterangan = $result->Keterangan_Normal_laki;
											} elseif ($jenis_kelamin === 'P') {
												$keterangan = $result->Keterangan_Normal_perempuan;
											}
										} elseif ($growth_data_for_usia->height_result === '>') {
											if ($jenis_kelamin === 'L') {
												$keterangan = $result->Keterangan_Tinggi_laki;
											} elseif ($jenis_kelamin === 'P') {
												$keterangan = $result->Keterangan_Tinggi_perempuan;
											}
										}
									}
								}
								echo $keterangan;
								?>
							</td>
							<td class="text-center">
								<?php
								$keterangan = '';
								if (!empty($growth_data_for_usia)) {
									$usia = $growth_data_for_usia->usia;
									$jenis_kelamin = $child_data->Jenis_kelamin; // Assuming you have stored gender in $data
									$this->db->where('Usia', $usia);
									$this->db->select('*');
									$this->db->limit(1);
									$result = $this->db->get('weight_nutrients')->row();
									if (!empty($result)) {
										if ($growth_data_for_usia->weight_result === '<') {
											if ($jenis_kelamin === 'L') {
												$keterangan = $result->Keterangan_Kurus_laki;
											} elseif ($jenis_kelamin === 'P') {
												$keterangan = $result->Keterangan_Kurus_perempuan;
											}
										} elseif ($growth_data_for_usia->weight_result === '=') {
											if ($jenis_kelamin === 'L') {
												$keterangan = $result->Keterangan_Normal_laki;
											} elseif ($jenis_kelamin === 'P') {
												$keterangan = $result->Keterangan_Normal_perempuan;
											}
										} elseif ($growth_data_for_usia->weight_result === '>') {
											if ($jenis_kelamin === 'L') {
												$keterangan = $result->Keterangan_Gendut_laki;
											} elseif ($jenis_kelamin === 'P') {
												$keterangan = $result->Keterangan_Gendut_perempuan;
											}
										}
									}
								}
								echo $keterangan;
								?>
							</td>
							<td class="text-center">
								<?php
								$keterangan = '';

								if (!empty($growth_data_for_usia)) {
									$panjang_badan = $growth_data_for_usia->panjang;
									$jenis_kelamin = $child_data->Jenis_kelamin; // Assuming you have stored gender in $data

									$this->db->where('Tinggi <=', $panjang_badan);


									$this->db->select('*');
									$this->db->limit(1);

									$result = $this->db->get('ideal_nutrients')->row();


									if (!empty($result)) {
										if ($growth_data_for_usia->weight_result === '<') {
											if ($jenis_kelamin === 'L') {
												$keterangan = $result->Keterangan_Kurus_laki;
											} elseif ($jenis_kelamin === 'P') {
												$keterangan = $result->Keterangan_Kurus_perempuan;
											}
										} elseif ($growth_data_for_usia->weight_result === '=') {
											if ($jenis_kelamin === 'L') {
												$keterangan = $result->Keterangan_Normal_laki;
											} elseif ($jenis_kelamin === 'P') {
												$keterangan = $result->Keterangan_Normal_perempuan;
											}
										} elseif ($growth_data_for_usia->weight_result === '>') {
											if ($jenis_kelamin === 'L') {
												$keterangan = $result->Keterangan_Gendut_laki;
											} elseif ($jenis_kelamin === 'P') {
												$keterangan = $result->Keterangan_Gendut_perempuan;
											}
										}
									}
								}

								echo $keterangan;
								?>
							</td>



						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
		<br>
		<br>
		<div class="invoice-price">
			<div class="invoice-price-left">
				<div class="invoice-price-row">
					<div class="sub-price">
						<small>Kenaikan Berat Badan </small>
						<span class="text-inverse"> <?php
													if ($berat_terakhir !== null) {
														$berat_badan_lahir = $child_data->Berat_badan_lahir;
														$berat_terakhir = $berat_terakhir - $berat_badan_lahir;
														echo ($berat_terakhir > 0 ? '+' : '') . $berat_terakhir . ' kg';
													} else {
														echo 'Data pertumbuhan belum tersedia.';
													}
													?></span>
					</div>
					<div class="sub-price">
						<i class="fa fa-times text-muted"></i>
					</div>
					<div class="sub-price">
						<small>Kenaikan Panjang Badan</small>
						<span class="text-inverse"><?php
													if (isset($panjang_terakhir) !== null) {
														$panjang_badan_lahir = $child_data->Panjang_badan_lahir;
														$panjang_terakhir = 0;
														// Now you can perform the subtraction
														$panjang_terakhir = $panjang_terakhir - $panjang_badan_lahir;

														echo ($panjang_terakhir > 0 ? '+' : '') . $panjang_terakhir . ' cm';
													} else {
														echo 'Data pertumbuhan belum tersedia.';
													}
													?></span>
					</div>
				</div>
			</div>

		</div>
		<!-- END invoice-price -->
	</div>
	<!-- END invoice-content -->
	<!-- BEGIN invoice-note -->
	<div class="invoice-note">
		<?= $this->db->get_where('settings', ['name' => 'app_name'])->row()->value ?> <br>
		<?= $this->db->get_where('settings', ['name' => 'address'])->row()->value ?>
	</div>
	<!-- END invoice-note -->
	<!-- BEGIN invoice-footer -->
	<div class="invoice-footer">
		<p class="text-center mb-5px fw-bold">
			TERIMA KASIH
		</p>
		<p class="text-center">
			<span class="me-10px"><i class="fa fa-fw fa-lg fa-globe"></i> matiasgallipoli.com</span>
			<span class="me-10px"><i class="fa fa-fw fa-lg fa-phone-volume"></i> T:016-18192302</span>
			<span class="me-10px"><i class="fa fa-fw fa-lg fa-envelope"></i> rtiemps@gmail.com</span>
		</p>
	</div>
	<!-- END invoice-footer -->
</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>

<script>
	window.onafterprint = function() {
		var printButton = document.getElementById("print-button");
		if (printButton) {
			printButton.style.display = "block";
		}
	};
</script>