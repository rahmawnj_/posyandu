<?php
$title = 'Master Anak';
$breadcrumb = ['Master Anak', 'Tambah Anak'];

$css_urls = [];
$js_urls = [];

ob_start();

?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
		<?php echo validation_errors(); ?>
		<?php echo form_open('childs/store'); ?>
		<fieldset>
			<?php if ($this->session->userdata('role') == 'Admin') : ?>
				<div class="form-group">
					<label for="id_parent" class="form-label">Ortu :</label>
					<select name="id_parent" class="form-control" id="id_parent">
						<?php foreach ($this->db->where('role', 'Parent')->get('users')->result() as $parent) : ?>
							<option value="<?= $parent->id ?>" <?= set_select('id_parent', $parent->id, $parent->id == set_value('id_parent')) ?>>
								<?= $parent->nama ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			<?php else : ?>
				<input type="hidden" name="id_parent" value="<?= $this->session->userdata('parent_id') ?>" class="form-control" id="id_parent" placeholder="Masukkan id_parent" />
			<?php endif ?>
			<div class="form-group">
				<label for="Nama" class="form-label">Nama:</label>
				<input type="text" name="Nama" value="<?php echo set_value('Nama'); ?>" class="form-control" id="Nama" placeholder="Masukkan Nama" />
			</div>
			<div class="form-group">
				<label for="code" class="form-label">Code:</label>
				<input type="text" name="code" value="<?php echo set_value('code'); ?>" class="form-control" id="code" placeholder="Masukkan code" />
			</div>
			<div class="form-group">
				<label for="Tanggal_lahir" class="form-label">Tanggal Lahir:</label>
				<input type="date" name="Tanggal_lahir" value="<?php echo set_value('Tanggal_lahir'); ?>" class="form-control" id="Tanggal_lahir" />
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="Berat_badan_lahir" class="form-label">Berat Badan Lahir (kg):</label>
						<div class="input-group">
							<input type="text" name="Berat_badan_lahir" value="<?php echo set_value('Berat_badan_lahir'); ?>" class="form-control" id="Berat_badan_lahir" placeholder="Masukkan Berat Badan Lahir" />
							<span class="input-group-text">kg</span>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="Panjang_badan_lahir" class="form-label">Panjang Badan Lahir (cm):</label>
						<div class="input-group">
							<input type="text" name="Panjang_badan_lahir" value="<?php echo set_value('Panjang_badan_lahir'); ?>" class="form-control" id="Panjang_badan_lahir" placeholder="Masukkan Panjang Badan Lahir" />
							<span class="input-group-text">cm</span>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="Jenis_kelamin" class="form-label">Jenis Kelamin:</label>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="Jenis_kelamin" value="P" <?php echo set_radio('Jenis_kelamin', 'P'); ?> id="jk_p">
					<label class="form-check-label" for="jk_p">P</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="Jenis_kelamin" value="L" <?php echo set_radio('Jenis_kelamin', 'L'); ?> id="jk_l">
					<label class="form-check-label" for="jk_l">L</label>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
			<?php echo form_close(); ?>
		</fieldset>
	</div>


</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>