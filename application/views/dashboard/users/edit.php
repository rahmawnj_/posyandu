<?php
$title = 'Master User';
$breadcrumb = ['Master User', 'Edit user'];

$css_urls = [];
$js_urls = [];

ob_start();

?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
		<?php echo validation_errors(); ?>

		<?php echo form_open_multipart('users/update/' . $user->id); ?>
		<fieldset>
			<div class="form-group">
				<label for="nama" class="form-label">Nama:</label>
				<input type="text" name="nama" value="<?php echo set_value('nama', $user->nama); ?>" class="form-control" id="nama" placeholder="Masukkan Nama" />
			</div>
			<div class="form-group">
				<label for="email" class="form-label">email:</label>
				<input type="text" name="email" value="<?php echo set_value('email', $user->email); ?>" class="form-control" id="email" placeholder="Masukkan email" />
			</div>
			<div class="form-group">
				<label for="password" class="form-label">Password:</label>
				<input type="password" name="password" class="form-control" id="password" placeholder="Masukkan Password" />
			</div>
			<div class="form-group">
				<label for="foto" class="form-label">Foto:</label>
				<input type="file" name="foto" class="form-control" id="foto" />
			</div>
			<div class="mb-1">
				<?php if (!empty($user->foto)) : ?>
					<img class="rounded" src="<?php echo base_url('assets/upload/image/users/' . $user->foto); ?>" width="100" height="100">
				<?php endif; ?>
			</div>
			<button type="submit" class="btn btn-primary me-2">Submit</button>
			<button type="button" class="btn btn-secondary" onclick="window.location.reload();">Cancel</button>
		</fieldset>
		<?php echo form_close(); ?>

	</div>


</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>
