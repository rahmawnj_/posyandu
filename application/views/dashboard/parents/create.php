<?php
$title = 'Master OrangTua';
$breadcrumb = ['Master OrangTua', 'Tambah OrangTua'];

$css_urls = [];
$js_urls = [];

ob_start();

?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
		<?php echo validation_errors(); ?>

		<?php echo form_open_multipart('parents/store'); ?>
		<fieldset>
			<div class="form-group">
				<label for="Nama_ayah" class="form-label">Nama Ayah:</label>
				<input type="text" name="Nama_ayah" value="<?php echo set_value('Nama_ayah'); ?>" class="form-control" id="Nama_ayah" placeholder="Masukkan Nama Ayah" />
			</div>
			<div class="form-group">
				<label for="Nama_ibu" class="form-label">Nama Ibu:</label>
				<input type="text" name="Nama_ibu" value="<?php echo set_value('Nama_ibu'); ?>" class="form-control" id="Nama_ibu" placeholder="Masukkan Nama Ibu" />
			</div>
			<div class="form-group">
				<label for="Alamat" class="form-label">Alamat:</label>
				<textarea name="Alamat" class="form-control" id="Alamat" placeholder="Masukkan Alamat"><?php echo set_value('Alamat'); ?></textarea>
			</div>
			<div class="form-group">
				<label for="id_user" class="form-label">User:</label>
				<select name="id_user" class="form-control" id="id_user">
					<?php
					$this->db->select('users.id, users.nama');
					$this->db->from('users');
					$this->db->join('parents', 'users.id = parents.id_user', 'LEFT');
					$this->db->where('users.role', 'Parent');
					$this->db->where('parents.id_user IS NULL', null, false);
					$query = $this->db->get();

					foreach ($query->result() as $user) : ?>
						<option value="<?php echo $user->id; ?>"><?php echo $user->nama; ?></option>
					<?php endforeach; ?>
				</select>

			</div>

			<button type="submit" class="btn btn-primary me-2">Submit</button>
			<button type="button" class="btn btn-secondary" onclick="window.location.reload();">Cancel</button>
		</fieldset>
		<?php echo form_close(); ?>
	</div>


</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>
