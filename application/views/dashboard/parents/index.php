<?php
$title = 'Master OrangTua';
$breadcrumb = ['Master OrangTua'];

$css_urls = [
	base_url('assets/d/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css'),
	base_url('assets/d/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css'),
	base_url('assets/d/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css'),
];

$js_urls = [
	base_url('assets/d/plugins/datatables.net/js/jquery.dataTables.min.js'),
	base_url('assets/d/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js'),
	base_url('assets/d/plugins/datatables.net-responsive/js/dataTables.responsive.min.js'),
	base_url('assets/d/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/dataTables.buttons.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.colVis.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.flash.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.html5.min.js'),
	base_url('assets/d/plugins/datatables.net-buttons/js/buttons.print.min.js'),
	base_url('assets/d/plugins/pdfmake/build/pdfmake.min.js'),
	base_url('assets/d/plugins/sweetalert/dist/sweetalert.min.js'),

	base_url('assets/d/plugins/pdfmake/build/vfs_fonts.js'),
	base_url('assets/d/plugins/jszip/dist/jszip.min.js'),
	base_url('assets/d/js/demo/table-manage-buttons.demo.js'),
	base_url('assets/d/plugins/@highlightjs/cdn-assets/highlight.min.js'),
	base_url('assets/d/js/demo/render.highlight.js'),

];


ob_start();

?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>

			<a href="<?= base_url('parents/create') ?>" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>

		</div>
	</div>
	<div class="panel-body">
		<table id="data-table" class="table table-striped table-bordered align-middle">
			<thead>
				<tr>
					<th width="1%"></th>
					<th class="text-nowrap">Nama Ayah</th>
					<th class="text-nowrap">Nama Ibu</th>
					<th class="text-nowrap">Alamat</th>
					<th class="text-nowrap">User</th>
					<th class="text-nowrap">Jumlah Anak</th>
					<th class="text-nowrap"></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($parents as $key => $parent) : ?>
					<tr class="<?= ($key % 2 == 0) ? 'even' : 'odd' ?>">
						<td class="fw-bold text-inverse"><?= $key + 1 ?></td>
						<td><?= $parent->Nama_ayah ?></td>
						<td><?= $parent->Nama_ibu ?></td>
						<td><?= $parent->Alamat ?></td>
						<td><?= $parent->user_nama ?></td>
						<td><?= $parent->child_count ?></td> <!-- Menampilkan jumlah anak -->

						<td>
							<a href="<?= site_url('parents/edit/' . $parent->id); ?>" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
							<a href="<?= site_url('parents/delete/' . $parent->id); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this parent?')"><i class="fas fa-trash-alt"></i></a>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>

	</div>
</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>
<script>
	$(document).ready(function() {
		// DataTable initialization
		var dataTable = $('#data-table').DataTable({
			// Add any DataTable configuration options you need
			dom: 'Bfrtip', // Add DataTables Buttons to the DOM
			buttons: [
				'copy', 'print'
			]
		});

	});
</script>


<?php if ($this->session->flashdata('success')) : ?>
	<script>
		swal({
			title: 'Success',
			text: '<?= $this->session->flashdata('success') ?>',
			icon: 'success',
			buttons: {
				confirm: {
					text: 'OK',
					className: 'btn btn-primary'
				}
			}
		});
	</script>
<?php endif; ?>
<?php if ($this->session->flashdata('error')) : ?>
	<script>
		swal({
			title: 'error',
			text: '<?= $this->session->flashdata('error') ?>',
			icon: 'error',
			buttons: {
				confirm: {
					text: 'OK',
					className: 'btn btn-primary'
				}
			}
		});
	</script>
<?php endif; ?>