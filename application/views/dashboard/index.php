<?php
$title = 'Dashboard';
$breadcrumb = ['Dashboard'];

$css_urls = [
];

$js_urls = [
	base_url('assets/d/plugins/apexcharts/dist/apexcharts.min.js'),
];

ob_start();
?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<div class="row">
	<!-- BEGIN col-3 -->
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats bg-blue">
			<div class="stats-icon"><i class="fa fa-desktop"></i></div>
			<div class="stats-info">
				<h4>Akun Admin</h4>
				<p><?= count($this->db->where('role', 'Admin')->get('users')->result()) ?></p>
			</div>
			<div class="stats-link">
				<a href="<?= base_url('users?role=Admin') ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats bg-red">
			<div class="stats-icon"><i class="fa fa-clock"></i></div>
			<div class="stats-info">
				<h4>Pengguna</h4>
				<p><?= count($this->db->where('role', 'Parent')->get('users')->result()) ?></p>

			</div>
			<div class="stats-link">
				<a href="<?= base_url('users?role=Parent') ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats bg-info">
			<div class="stats-icon"><i class="fa fa-link"></i></div>
			<div class="stats-info">
				<h4>Orang Tua</h4>
				<p><?= count($this->db->get('parents')->result()) ?></p>
			</div>
			<div class="stats-link">
				<a href="<?= base_url('parents') ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<!-- END col-3 -->
	<!-- BEGIN col-3 -->
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats bg-orange">
			<div class="stats-icon"><i class="fa fa-users"></i></div>
			<div class="stats-info">
				<h4>Anak</h4>
				<p><?= count($this->db->get('childs')->result()) ?></p>
			</div>
			<div class="stats-link">
				<a href="<?= base_url('childs') ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<!-- END col-3 -->
	<!-- BEGIN col-3 -->

	<!-- END col-3 -->
</div>
<!-- END row -->

<div class="row">
	<!-- BEGIN col-8 -->
	<div class="col-xl-8">
		<div class="widget-chart with-sidebar">
			<div class="widget-chart-content bg-white">
				<h4 class="chart-title">
					Analisis Perkembangan
					<small>1 bulan terakhir</small>
				</h4>
				<div id="chart" class="widget-chart-full-width" style="height: 220px;"></div>
			</div>
			<div class="widget-chart-sidebar bg-white">
				<div class="chart-number">
					<small>Total Penimbangan</small>
				</div>
				<div class="flex-grow-1 d-flex align-items-center">
					<div id="chart-weight-results-by-age" style="height: 10px"></div>
				</div>

			</div>
		</div>
	</div>
	<!-- END col-8 -->
	<!-- BEGIN col-4 -->
	<div class="col-xl-4">
		<div class="panel panel-default" data-sortable-id="index-1">
			<div class="panel-heading">
				<h4 class="panel-title">
					Perbandingan Gender
				</h4>
			</div>
			<div class="mb-3 text-gray-200">
				<b></b>
				<span class="ms-2"><i class="fa fa-info-circle" data-bs-toggle="popover" data-bs-trigger="hover" data-bs-title="Perbandingan Gender" data-bs-placement="top" data-bs-content="Data Penimbangan berdasarkan gender dalam 1 bulan terakhir."></i></span>
			</div>
			<!-- END title -->
			<!-- END row -->
			<hr class="bg-white-transparent-2 mt-20px mb-20px" />
			<!-- BEGIN row -->
			<div class="row align-items-center mb-40px">
				<div id="chart-gender"></div>
			</div>
		</div>
	</div>
	<!-- END col-4 -->
</div>
<!-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> -->

<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>
<script>
	var options = {
		chart: {
			type: 'bar',
			height: 350,
		},
		plotOptions: {
			bar: {
				horizontal: false,
			},
		},
		series: [{
			name: 'Statistik',
			data: [<?= $averageWeight; ?>, <?= $averageHeight; ?>, <?= $averageBMI; ?>],
		}, ],
		xaxis: {
			categories: ['Rata-rata Berat Badan', 'Rata-rata Tinggi Badan', 'Rata-rata BMI'],
		},
	};

	var chart = new ApexCharts(document.querySelector("#chart"), options);
	chart.render();
</script>

<?php
	$this->db->select('*');
	$this->db->join('childs', 'childs.id = histories.id_child');
	$this->db->where('DATEDIFF(CURDATE(), childs.Tanggal_lahir) <=', 60 * 30); // Maksimal 60 bulan (30 hari per bulan)
	$histories = $this->db->get('histories')->result();

	$this->db->select('*');
	$this->db->where('DATEDIFF(CURDATE(), Tanggal_lahir) <=', 60 * 30); // Maksimal 60 bulan (30 hari per bulan)
	$childs = $this->db->get('childs')->result();

	$totalChilds = count($childs);
	$totalTimbangan = 0;

	foreach ($childs as $child) {
		// Hitung berapa banyak anak yang telah melakukan timbangan
		if ($child->is_on == 1) { // Anda mungkin perlu menyesuaikan kondisi ini dengan cara Anda menyimpan data
			$totalTimbangan++;
		}
	}

	$persentaseTimbangan = ($totalTimbangan / $totalChilds) * 100;
?>


<script>
	var anakTerdaftar = <?= count($childs) ?>; // Total anak terdaftar
	var anakTimbang = <?= count($histories) ?>; // Jumlah anak yang telah melakukan timbangan
	var progress = Math.round((anakTimbang / anakTerdaftar) * 100);

	var options = {
		chart: {
			type: 'radialBar',
			height: 200,
		},
		plotOptions: {
			radialBar: {
				hollow: {
					size: '40%',
				},
				dataLabels: {
					showOn: 'always',
				},
			},
		},
		series: [progress],
		labels: ['Progress'],
	};

	var chart = new ApexCharts(document.querySelector("#progress-chart"), options);
	chart.render();
</script>

<script>
	var weightResultsByAgeGroup = {
		"0-12 bulan": <?= count(array_filter($histories, function ($history) {
							return $history->usia <= 12;
						})); ?>,
		"13-24 bulan": <?= count(array_filter($histories, function ($history) {
							return $history->usia >= 13 && $history->usia <= 24;
						})); ?>,
		"25-36 bulan": <?= count(array_filter($histories, function ($history) {
							return $history->usia >= 25 && $history->usia <= 36;
						})); ?>,
		"37-48 bulan": <?= count(array_filter($histories, function ($history) {
							return $history->usia >= 37 && $history->usia <= 48;
						})); ?>,
		"49-60 bulan": <?= count(array_filter($histories, function ($history) {
							return $history->usia >= 49 && $history->usia <= 60;
						})); ?>,
	};

	var optionsWeightResultsByAge = {
		chart: {
			type: 'bar',
			height: 350,
		},
		plotOptions: {
			bar: {
				horizontal: false,
			},
		},
		series: [{
			name: 'Hasil Timbangan',
			data: Object.values(weightResultsByAgeGroup),
		}],
		xaxis: {
			categories: Object.keys(weightResultsByAgeGroup),
		},
		yaxis: {
			labels: {
				formatter: function(val) {
					return parseInt(val);
				}
			}
		},
	};

	var chartWeightResultsByAge = new ApexCharts(document.querySelector("#chart-weight-results-by-age"), optionsWeightResultsByAge);
	chartWeightResultsByAge.render();
</script>


<script>
	var optionsGender = {
		chart: {
			type: 'donut',
			height: 300,
		},
		legend: {
					position: 'bottom', 
				},
		plotOptions: {
			pie: {
				donut: {
					size: '70%',
				},
				labels: {
					position: 'bottom', 
				},
			},
		},
		series: [
			<?= count(array_filter($childs, function ($child) {
				return $child->Jenis_kelamin === 'L';
			})); ?>,
			<?= count(array_filter($childs, function ($child) {
				return $child->Jenis_kelamin === 'P';
			})); ?>, 
		],
		labels: ['Laki-laki', 'Perempuan'],
	};

	var chartGender = new ApexCharts(document.querySelector("#chart-gender"), optionsGender);
	chartGender.render();
</script>
