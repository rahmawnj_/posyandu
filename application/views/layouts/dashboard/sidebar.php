<div id="sidebar" class="app-sidebar" style="background-color: white;">
	<!-- BEGIN scrollbar -->
	<div class="app-sidebar-content" data-scrollbar="true" data-height="100%">
		<!-- BEGIN menu -->
		<div class="menu">
			<div class="menu-profile">
				<a href="<?= base_url('auth/profile') ?>" class="menu-profile-link" data-toggle="app-sidebar-profile" data-target="#appSidebarProfileMenu">
					<div class="menu-profile-cover with-shadow"></div>
					<div class="menu-profile-image">
						<img src="<?= base_url('assets/upload/image/users/' . $this->session->userdata('foto')); ?>" alt="" />
					</div>
					<div class="menu-profile-info">
						<div class="d-flex align-items-center">
							<div class="flex-grow-1">
								<?= $this->session->userdata('nama'); ?>
							</div>
							<div class="menu-caret ms-auto"></div>
						</div>
						<small><?= $this->session->userdata('role'); ?></small>
					</div>
				</a>
			</div>

			<div id="appSidebarProfileMenu" class="collapse ">
				<?php if ($this->session->userdata('role') == 'Admin') : ?>
					<div class="menu-item pt-5px <?= ($title === 'Pengaturan') ? 'active' : '' ?>">
						<a href="<?= base_url('settings') ?>" class="menu-link">
							<div class="menu-icon"><i class="fa fa-cog"></i></div>
							<div class="menu-text">Settings</div>
						</a>
					</div>
				<?php endif ?>
				<div class="menu-item pt-5px <?= ($title === 'Profile') ? 'active' : '' ?>">
					<a href="<?= base_url('auth/profile') ?>" class="menu-link">
						<div class="menu-icon"><i class="fa fa-cog"></i></div>
						<div class="menu-text">Profile</div>
					</a>
				</div>

				<div class="menu-divider m-0"></div>
			</div>


			<div class="menu-header">Menu Utama</div>


			<?php if ($this->session->userdata('role') == 'Admin') : ?>


				<div class="menu-item <?= ($title === 'Dashboard') ? 'active' : '' ?>">
					<a href="<?= base_url('dashboard') ?>" class="menu-link">
						<div class="menu-icon">
							<i class="fas fa-tachometer-alt"></i>
						</div>
						<div class="menu-text">Dashboard</div>
					</a>
				</div>
				<div class="menu-item has-sub <?= ($title === 'Gizi') ? 'active' : '' ?>">
					<a href="javascript:;" class="menu-link <?php if (($breadcrumb ?? null) && in_array('Berat Badan', $breadcrumb)) echo 'active'; ?>">
						<div class="menu-icon">
							<i class="fas fa-clipboard-list"></i>
						</div>
						<div class="menu-text">Manajemen</div>
						<div class="menu-caret"></div>
					</a>
					<div class="menu-submenu">
						<div class="menu-item <?php if (current_url() == base_url('nutrients/weights')) echo 'active'; ?>"">
							<a href=" <?= base_url('nutrients/weights') ?>" class="menu-link">
							<div class="menu-text">Berat</div>
							</a>
						</div>
						<div class="menu-item <?php if (current_url() == base_url('nutrients/heights')) echo 'active'; ?>"">
							<a href=" <?= base_url('nutrients/heights') ?>" class="menu-link">
							<div class="menu-text">Panjang</div>
							</a>
						</div>
						<div class="menu-item <?php if (current_url() == base_url('nutrients/ideals')) echo 'active'; ?>"">
							<a href=" <?= base_url('nutrients/ideals') ?>" class="menu-link">
							<div class="menu-text">Panjang x Berat</div>
							</a>
						</div>
					</div>
				</div>
			<?php endif ?>

			<div class="menu-item has-sub <?= ($title === 'Grafik Gizi') ? 'active' : '' ?> ">
				<a href="javascript:;" class="menu-link">
					<div class="menu-icon">
						<i class="fas fa-chart-line"></i>
					</div>
					<div class="menu-text">Grafik</div>
					<div class="menu-caret"></div>
				</a>
				<div class="menu-submenu">
					<div class="menu-item has-sub <?php if (($breadcrumb ?? null) && in_array('Tinggi', $breadcrumb)) echo 'active'; ?>">
						<a href="javascript:;" class="menu-link">
							<div class="menu-text">Tinggi</div>
							<div class="menu-caret"></div>
						</a>
						<div class="menu-submenu">
							<div class="menu-item <?php if (current_url() == base_url('nutrients/height_boy_graph')) echo 'active'; ?>">
								<a href="<?= base_url('nutrients/height_boy_graph') ?>" class="menu-link">
									<div class="menu-text">Laki - laki</div>
								</a>
							</div>
							<div class="menu-item <?php if (current_url() == base_url('nutrients/height_girl_graph')) echo 'active'; ?>">
								<a href="<?= base_url('nutrients/height_girl_graph') ?>" class="menu-link ">
									<div class="menu-text">Perempuan</div>
								</a>
							</div>
						</div>
					</div>
					<div class="menu-item has-sub  <?php if (($breadcrumb ?? null) && in_array('Berat Badan', $breadcrumb)) echo 'active'; ?>">
						<a href="javascript:;" class="menu-link">
							<div class="menu-text">Berat Badan</div>
							<div class="menu-caret"></div>
						</a>
						<div class="menu-submenu">

							<div class="menu-item <?php if (current_url() == base_url('nutrients/weight_boy_graph')) echo 'active'; ?>"><a href="<?= base_url('nutrients/weight_boy_graph') ?>" class="menu-link">
									<div class="menu-text">Laki - laki</div>
								</a></div>
							<div class="menu-item <?php if (current_url() == base_url('nutrients/weight_girl_graph')) echo 'active'; ?>"><a href="<?= base_url('nutrients/weight_girl_graph') ?>" class="menu-link">
									<div class="menu-text">Perempuan</div>
								</a></div>
						</div>
					</div>

				</div>
			</div>

			<div class="menu-header">Data Master</div>
			<div class="menu-item <?= ($title === 'Master Anak') ? 'active' : '' ?>">
				<a href="<?= base_url('childs') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fa fa-child"></i>
					</div>
					<div class="menu-text">Anak</div>
				</a>
			</div>
			<?php if ($this->session->userdata('role') == 'Admin') : ?>
				<div class="menu-item <?= ($title === 'Master OrangTua') ? 'active' : '' ?>">
					<a href="<?= base_url('parents') ?>" class="menu-link">
						<div class="menu-icon">
							<i class="fas fa-user-friends"></i>
						</div>
						<div class="menu-text">Orang Tua</div>
					</a>
				</div>

				<div class="menu-item has-sub <?= ($title === 'Master User') ? 'active' : '' ?>">
					<a href="javascript:;" class="menu-link">
						<div class="menu-icon">
							<i class="fa fa-user"></i>
						</div>
						<div class="menu-text">Pengguna</div>
						<div class="menu-caret"></div>
					</a>
					<div class="menu-submenu">
						<div class="menu-item <?= (current_url('?role=Admin') == base_url('users?role=Admin')) ? 'active' : '' ?>">
							<a href="<?= base_url('users?role=Admin') ?>" class="menu-link">
								<div class="menu-text">Admin</div>
							</a>
						</div>

						<div class="menu-item <?= (current_url('?role=Parent') == base_url('users?role=Parent')) ? 'active' : '' ?>">
							<a href="<?= base_url('users?role=Parent') ?>" class="menu-link">
								<div class="menu-text">Ortu</div>
							</a>
						</div>
					</div>

				</div>
			<?php endif ?>

			<div class="menu-header">Riwayat & Laporan</div>


			<div class="menu-item <?= ($title === 'Riwayat') ? 'active' : '' ?>">
				<a href="<?= base_url('histories') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fas fa-file-alt"></i>
					</div>
					<div class="menu-text">Riwayat Timbangan</div>
				</a>
			</div>

			<!-- BEGIN minify-button -->
			<div class="menu-item d-flex">
				<a href="javascript:;" class="app-sidebar-minify-btn ms-auto" data-toggle="app-sidebar-minify"><i class="fa fa-angle-double-left"></i></a>
			</div>
			<!-- END minify-button -->
		</div>
		<!-- END menu -->
	</div>
	<!-- END scrollbar -->
</div>
<div class="app-sidebar-bg"></div>
<div class="app-sidebar-mobile-backdrop"><a href="#" data-dismiss="app-sidebar-mobile" class="stretched-link"></a></div>