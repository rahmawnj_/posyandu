<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title><?= $this->db->get_where('settings', ['name' => 'app_name'])->row()->value?><?= isset($title) ? ' | ' . $title : '' ?></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />

	<!-- ================== BEGIN core-css ================== -->
	<link href="<?= base_url('assets/d/css/vendor.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/d/css/facebook/app.min.css') ?>" rel="stylesheet" />
	<!-- ================== END core-css ================== -->
    <link rel="icon" type="image/png" href="<?= base_url('assets/upload/image/settings/' . $this->db->get_where('settings', ['name' => 'logo'])->row()->value ?? ''); ?>" />

	<?php foreach ($css_urls as $css_url) : ?>
		<link href="<?= $css_url ?>" rel="stylesheet" />
	<?php endforeach; ?>
</head>

<body>

	<!-- BEGIN #app -->
	<div id="app" class="app app-header-fixed app-sidebar-fixed">
		<?php $this->load->view('layouts/dashboard/headbar') ?>
		<?php $this->load->view('layouts/dashboard/sidebar') ?>

		<div id="content" class="app-content">
			<?= $content ?>
			<?php $this->load->view('layouts/dashboard/footbar') ?>
		</div>

		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top" data-toggle="scroll-to-top"><i class="fa fa-angle-up"></i></a>
	</div>

	<script src="<?= base_url('assets/d/js/vendor.min.js') ?>"></script>
	<script src="<?= base_url('assets/d/js/app.min.js') ?>"></script>
	<script src="<?= base_url('assets/d/js/theme/facebook.min.js') ?>"></script>
	<?php foreach ($js_urls as $js_url) : ?>
		<script src="<?= $js_url ?>"></script>
	<?php endforeach; ?>
</body>

</html>
