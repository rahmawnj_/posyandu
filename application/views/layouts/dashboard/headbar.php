<div id="header" class="app-header app-header-inverse">
	<!-- BEGIN navbar-header -->
	<div class="navbar-header">

		<?php
		$logoValue = $this->db->get_where('settings', ['name' => 'logo'])->row()->value;

		$logoURL = !empty($logoValue)
			? base_url('assets/upload/image/settings/' . $logoValue)
			: false;
		?>

		<a href="<?= base_url('/') ?>" class="navbar-brand">
			<?php if ($logoURL) : ?>
				<img src="<?= $logoURL ?>" alt="Logo" style="max-height: 30px; max-width: 30px;" />
			<?php else : ?>
				<i class="fa fa-universal-access fa-lg"></i>
			<?php endif; ?>
			<b><?= $this->db->get_where('settings', ['name' => 'app_name'])->row()->value ?></b>
		</a>

		<!-- <a href="index.html" class="navbar-brand"><i class="fa fa-universal-access fa-lg"></i> <b><?= $this->db->get_where('settings', ['name' => 'app_name'])->row()->value ?></b></a> -->
		<button type="button" class="navbar-mobile-toggler" data-toggle="app-sidebar-mobile">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<!-- END navbar-header -->
	<!-- BEGIN header-nav -->
	<div class="navbar-nav">
		<div class="navbar-item navbar-form">
			<div class="form-group">
				<input id="search-input" type="text" class="form-control" placeholder="Search Menu" />
				<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
			</div>
		</div>
		<!-- <div class="navbar-item dropdown">
			<a href="#" data-bs-toggle="dropdown" class="navbar-link dropdown-toggle icon">
				<i class="fa fa-bell"></i>
				<span class="badge">5</span>
			</a>
			<div class="dropdown-menu media-list dropdown-menu-end">
				<div class="dropdown-header">NOTIFICATIONS (5)</div>
				<a href="javascript:;" class="dropdown-item media">
					<div class="media-left">
						<i class="fa fa-bug media-object bg-gray-400"></i>
					</div>
					<div class="media-body">
						<h6 class="media-heading">Server Error Reports <i class="fa fa-exclamation-circle text-danger"></i></h6>
						<div class="text-muted fs-10px">3 minutes ago</div>
					</div>
				</a>
				<a href="javascript:;" class="dropdown-item media">
					<div class="media-left">
						<img src="<?php base_url('assets/img/user/user-1.jpg') ?>" class="media-object" alt="" />
						<i class="fab fa-facebook-messenger text-blue media-object-icon"></i>
					</div>
					<div class="media-body">
						<h6 class="media-heading">John Smith</h6>
						<p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
						<div class="text-muted fs-10px">25 minutes ago</div>
					</div>
				</a>
				<a href="javascript:;" class="dropdown-item media">
					<div class="media-left">
						<img src="<?php base_url('assets/img/user/user-2.jpg') ?>" class="media-object" alt="" />
						<i class="fab fa-facebook-messenger text-blue media-object-icon"></i>
					</div>
					<div class="media-body">
						<h6 class="media-heading">Olivia</h6>
						<p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
						<div class="text-muted fs-10px">35 minutes ago</div>
					</div>
				</a>
				<a href="javascript:;" class="dropdown-item media">
					<div class="media-left">
						<i class="fa fa-plus media-object bg-gray-400"></i>
					</div>
					<div class="media-body">
						<h6 class="media-heading"> New User Registered</h6>
						<div class="text-muted fs-10px">1 hour ago</div>
					</div>
				</a>
				<a href="javascript:;" class="dropdown-item media">
					<div class="media-left">
						<i class="fa fa-envelope media-object bg-gray-400"></i>
						<i class="fab fa-google text-warning media-object-icon fs-14px"></i>
					</div>
					<div class="media-body">
						<h6 class="media-heading"> New Email From John</h6>
						<div class="text-muted fs-10px">2 hour ago</div>
					</div>
				</a>
				<div class="dropdown-footer text-center">
					<a href="javascript:;" class="text-decoration-none">View more</a>
				</div>
			</div>
		</div> -->
		<div class="navbar-item navbar-user dropdown">
			<a href="#" class="navbar-link dropdown-toggle d-flex align-items-center" data-bs-toggle="dropdown">
				<img src="<?= base_url('assets/upload/image/users/' . $this->session->userdata('foto')); ?>" alt="" />
				<span class="d-none d-md-inline"><?= $this->session->userdata('nama'); ?></span> <b class="caret ms-6px"></b>
			</a>
			<div class="dropdown-menu dropdown-menu-end me-1">
				<a href="<?= base_url('auth/profile') ?>" class="dropdown-item">Edit Profile</a>
				<?php if ($this->session->userdata('role') == 'Admin') : ?>

					<a href="<?= base_url('settings') ?>" class="dropdown-item">Setting</a>
				<?php endif ?>

				<div class="dropdown-divider"></div>
				<a href="<?= base_url('auth/logout') ?>" class="dropdown-item">Log Out</a>
			</div>
		</div>
	</div>
	<!-- END header-nav -->
</div>
<script>
	document.addEventListener("DOMContentLoaded", function() {
		// Dapatkan elemen input
		var searchInput = document.getElementById("search-input");

		// Dapatkan semua elemen menu yang ingin Anda filter
		var menuItems = document.querySelectorAll(".menu-item");

		// Tambahkan event listener untuk mengamati perubahan di input
		searchInput.addEventListener("input", function() {
			var keyword = searchInput.value.toLowerCase();
			console.log(searchInput);

			// Loop melalui semua elemen menu
			menuItems.forEach(function(menuItem) {
				var menuText = menuItem.querySelector(".menu-text");
				console.log(menuText);
				if (menuText) {
					var text = menuText.textContent.toLowerCase();

					// Periksa apakah teks menu mengandung kata kunci
					if (text.includes(keyword)) {
						menuItem.style.display = "block";
					} else {
						menuItem.style.display = "none";
					}
				}
			});
		});
	});
</script>