<div class="header header-float">
	<div class="container d-flex">
		<div class="header-logo">
			<?php
			$logoValue = $this->db->get_where('settings', ['name' => 'logo'])->row()->value;
		
			$logoURL = !empty($logoValue)
				? base_url('assets/upload/image/settings/' . $logoValue)
				: false;


			?>
			<a href="<?= base_url('/') ?>" class="logo-link">
				<?php if ($logoURL) : ?>
					<img src="<?= $logoURL ?>" alt="Logo" style="max-height: 30px; max-width: 30px;" />
				<?php else : ?>
					<i class="fa fa-universal-access fa-lg"></i>
				<?php endif; ?>
				<b><?= $this->db->get_where('settings', ['name' => 'app_name'])->row()->value ?></b> </a>
		</div>
		<div class="header-nav">
			<!-- <div class="container">
				<div class="header-nav-item">
					<a href="about.html" class="header-nav-link">Who We Are</a>
				</div>
				<div class="header-nav-item">
					<a href="products.html" class="header-nav-link">Our Products</a>
				</div>
				<div class="header-nav-item">
					<a href="newsroom.html" class="header-nav-link">Newsroom</a>
				</div>
				<div class="header-nav-item">
					<a href="careers.html" class="header-nav-link">Our Careers</a>
				</div>
				<div class="header-nav-item">
					<a href="contact_us.html" class="header-nav-link">Contact Us</a>
				</div>
			</div> -->
		</div>
		<div class="header-btn">
			<?php if ($this->session->userdata('id')) : ?>
				<a href="<?= base_url('dashboard') ?>" class="btn btn-primary fw-bold rounded-pill"><?= $this->session->userdata('nama'); ?> <i class="fa fa-arrow-right ms-1 opacity-5"></i></a>
			<?php else : ?>
				<a href="<?= base_url('auth/signup') ?>" style="margin-right: 5px;" class="btn btn-primary fw-bold rounded">Register</a>
				<a href="<?= base_url('auth/signin') ?>" class="btn btn-outline-dark fw-bold rounded">Masuk</a>
			<?php endif; ?>
		</div>
		<button class="header-mobile-nav-toggler" type="button" data-toggle="header-mobile-nav">
			<span class="header-mobile-nav-toggler-icon"></span>
		</button>
	</div>
</div>
