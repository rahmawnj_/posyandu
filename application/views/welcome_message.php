<?php
// $title = '';
$css_urls = [];
$js_urls = [];

ob_start();
?>


<!-- BEGIN section -->
<div class="section section-hero">
	<!-- BEGIN section-bg -->
	<div class="section-bg with-cover" style="background-image: url(<?= base_url('assets/lp/img/ilustrasi-posyandu.png') ?>);"></div>
	<div class="section-bg bg-gray-800-transparent-5"></div>
	<!-- END section-bg -->

	<!-- BEGIN container -->
	<div class="container position-relative">
		<!-- BEGIN section-hero-content -->
		<div class="section-hero-content">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-8 -->
				<div class="col-lg-8 col-lg-10">
					<h1 class="hero-title mb-3 mt-5 pt-md-5">
						<?= $this->db->get_where('settings', ['name' => 'hero_description'])->row()->value ?? '' ?>
					</h1>
					<div class="fs-18px text-white-transparent-8">
						<?= $this->db->get_where('settings', ['name' => 'description'])->row()->value ?? '' ?>
					</div>

					<div class="row text-white mt-4 mb-5 pb-5">
						<div class="col-lg-4 mb-3 mb-lg-0">
							<div class="d-flex align-items-center">
								<div class="h1 text-white-transparent-3 me-3"><i class="bi bi-people"></i></div>
								<div>
									<div class="fw-bold mb-1 h3"><?= count($this->db->get('childs')->result_array()) ?></div>
									<div class="fw-bold text-white-transparent-8">Data Anak</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="d-flex align-items-center">
								<div class="h1 text-white-transparent-3 me-3"><i class="bi bi-heart"></i></div>
								<div>
									<div class="fw-bold mb-1 h3"><?= count($this->db->get('parents')->result_array()) ?> Keluarga</div>
									<div class="fw-bold text-white-transparent-8">Sudah Bergabung</div>
								</div>
							</div>
						</div>
					</div>

					<a href="<?= base_url('auth/signin') ?>" class="hero-btn fw-bold mb-n5"><i class="fa fa-arrow-right"></i> Pantau Kesehatan Anak</a>
				</div>

				<!-- END col-8 -->
			</div>
			<!-- END row -->
		</div>
		<!-- END section-hero-content -->
	</div>
	<!-- END container -->
</div>
<!-- END section -->

<!-- BEGIN section -->

<!-- END section -->


<?php
$content = ob_get_clean();
$this->load->view('layouts/landingpage/main', ['content' => $content, 'js_urls' => $js_urls, 'css_urls' => $css_urls]);
?>