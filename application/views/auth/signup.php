<?php
$title = 'Register';
$css_urls = [];
$js_urls = [];

ob_start();
?>

<div class="section bg-purple">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-6">
				<div class="card">
					<div class="card-header">Register</div>
					<div class="card-body">
						<form method="post" action="<?= base_url('auth/signup_store') ?>">
							<?php if (!empty(validation_errors())) : ?>
								<div class="alert alert-danger"><?= validation_errors(); ?></div>
							<?php endif; ?>

							<div class="form-group">
								<label for="email">Email</label>
								<input type="text" class="form-control" id="email" name="email" value="<?= set_value('email'); ?>" required>
							</div>
							<div class="form-group">
								<label for="nama">Nama Pengguna</label>
								<input type="text" class="form-control" id="nama" name="nama" value="<?= set_value('nama'); ?>" required>
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="password" name="password" required>
							</div>
							<div class="form-group">
								<label for="password_confirmation">Password Confirmation</label>
								<input type="password_confirmation" class="form-control" id="password_confirmation" name="password_confirmation" required>
							</div>

							<!-- Form for Parent Information -->
							<div class="form-group">
								<label for="nama_ayah">Nama Ayah</label>
								<input type="text" class="form-control" id="nama_ayah" name="nama_ayah" value="<?= set_value('nama_ayah'); ?>">
							</div>
							<div class="form-group">
								<label for="nama_ibu">Nama Ibu</label>
								<input type="text" class="form-control" id="nama_ibu" name="nama_ibu" value="<?= set_value('nama_ibu'); ?>">
							</div>

							<div class="form-group">
								<label for="alamat">Alamat</label>
								<textarea class="form-control" id="alamat" name="alamat"><?= set_value('alamat'); ?></textarea>
							</div>

							<button type="submit" class="btn btn-primary btn-block">Register</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$content = ob_get_clean();
$this->load->view('layouts/landingpage/main', ['content' => $content, 'title' => $title, 'js_urls' => $js_urls, 'css_urls' => $css_urls]);
?>
