<?php
$title = 'Profile';
$breadcrumb = ['Profile'];

$css_urls = [];
$js_urls = [];

ob_start();

?>

<!-- BEGIN breadcrumb -->
<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $crumb) : ?>
		<?php if ($key === count($breadcrumb) - 1) : ?>
			<li class="breadcrumb-item active"><?= ucfirst($crumb) ?></li>
		<?php else : ?>
			<li class="breadcrumb-item"><a href="javascript:;"><?= ucfirst($crumb) ?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
</ol>
<!-- END breadcrumb -->

<!-- BEGIN panel -->
<div class="panel panel-inverse">
	<div class="panel-heading">
		<h4 class="panel-title"><?= $title ?? '' ?></h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
		<?php echo validation_errors(); ?>

		<?php echo form_open_multipart('auth/submit_profile'); ?>
		<fieldset>
			<legend class="mb-3">Edit Profil Pengguna</legend>
			<div class="form-group">
				<label for="nama" class="form-label">Nama:</label>
				<input type="text" name="nama" value="<?php echo set_value('nama', $user->nama); ?>" class="form-control" id="nama" placeholder="Masukkan Nama" />
			</div>
			<div class="form-group">
				<label for="email" class="form-label">Email:</label>
				<input type="text" name="email" value="<?php echo set_value('email', $user->email); ?>" class="form-control" id="email" placeholder="Masukkan Email" />
			</div>
			<div class="form-group">
				<label for="password" class="form-label">Password:</label>
				<input type="password" name="password" class="form-control" id="password" placeholder="Masukkan Password" autocomplete="false" />
			</div>

			<div class="form-group">
				<label for="foto" class="form-label">Foto Profil:</label>
				<input type="file" name="foto" class="form-control" id="foto" />
			</div>
			<div class="mb-1">
				<?php if (!empty($user->foto)) : ?>
					<img class="rounded" src="<?php echo base_url('assets/upload/image/users/' . $user->foto); ?>" width="100" height="100">
				<?php endif; ?>
			</div>
			<?php if ($this->session->userdata('role') != 'Admin') : ?>

			<hr>
			<legend class="mb-3">Edit Profil Orang Tua</legend>
			<input type="hidden" name="parent_id" value="<?php echo set_value('parent_id', $parent->id); ?>" class="form-control" id="nama_ayah" placeholder="Masukkan Nama Ayah" />
			<div class="form-group">
				<label for="nama_ayah" class="form-label">Nama Ayah:</label>
				<input type="text" name="nama_ayah" value="<?php echo set_value('nama_ayah', $parent->Nama_ayah); ?>" class="form-control" id="nama_ayah" placeholder="Masukkan Nama Ayah" />
			</div>
			<div class="form-group">
				<label for="nama_ibu" class="form-label">Nama Ibu:</label>
				<input type="text" name="nama_ibu" value="<?php echo set_value('nama_ibu', $parent->Nama_ibu); ?>" class="form-control" id="nama_ibu" placeholder="Masukkan Nama Ibu" />
			</div>
			<div class="form-group">
				<label for="alamat" class="form-label">Alamat:</label>
				<textarea name="alamat" class="form-control" id="alamat" placeholder="Masukkan Alamat Orang Tua"><?php echo set_value('alamat', $parent->Alamat); ?></textarea>
			</div>
			
			<?php endif ?>
			<button type="submit" class="btn btn-primary me-2">Update Profil</button>
		</fieldset>
		<?php echo form_close(); ?>

	</div>


</div>
<!-- END panel -->
<?php
$content = ob_get_clean();
$this->load->view('layouts/dashboard/main', ['content' => $content, 'title' => $title, 'css_urls' => $css_urls, 'js_urls' => $js_urls]);
?>


<!-- views/auth/edit_profile.php -->